using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using NUnit.Framework;

namespace ORM.Tests
{
    public class UpsertTests : BaseTests
    {
        [Test]
        public void Upsert_InsertEntity()
        {
            // create and insert entity
            Teacher teacher = CreateTeacher();
            _context.Teachers.Upsert(teacher);

            string sqlQuery = "SELECT * FROM dbo.Teacher";
            SqlCommand command = new SqlCommand(sqlQuery, _connection);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Assert.AreEqual(1, reader.GetFieldValue<int>(0));
                Assert.AreEqual("Tom Truthahn", reader.GetFieldValue<string>(1));
                Assert.AreEqual(new DateTime(1990, 10, 10), reader.GetFieldValue<DateTime>(2));
                Assert.AreEqual(0, reader.GetFieldValue<int>(3));
            }
        }

        [Test]
        public void Upsert_UpdateEntity()
        {
            // create and insert entity
            Teacher teacher = CreateTeacher();
            _context.Teachers.Upsert(teacher);

            // update entity
            teacher.Name = "Tom Tintenfisch";
            _context.Teachers.Upsert(teacher);

            string sqlQuery = "SELECT * FROM dbo.Teacher";
            SqlCommand command = new SqlCommand(sqlQuery, _connection);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Assert.AreEqual(1, reader.GetFieldValue<int>(0));
                Assert.AreEqual("Tom Tintenfisch", reader.GetFieldValue<string>(1));
                Assert.AreEqual(new DateTime(1990, 10, 10), reader.GetFieldValue<DateTime>(2));
                Assert.AreEqual(0, reader.GetFieldValue<int>(3));
            }
        }

        [Test]
        public void Upsert_InsertEntityWithForeignKey()
        {
            // create and insert entity with foreign key
            Class c = CreateClass();
            _context.Classes.Upsert(c);

            string sqlQuery = "SELECT * FROM dbo.Class";
            SqlCommand command = new SqlCommand(sqlQuery, _connection);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Assert.AreEqual(1, reader.GetFieldValue<int>(0));
                Assert.AreEqual("AUDIMAX", reader.GetFieldValue<string>(1));
                Assert.AreEqual(1, reader.GetFieldValue<int>(2));
            }

            sqlQuery = "SELECT * FROM dbo.Teacher";
            command = new SqlCommand(sqlQuery, _connection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                Assert.AreEqual(1, reader.GetFieldValue<int>(0));
                Assert.AreEqual("Tom Truthahn", reader.GetFieldValue<string>(1));
                Assert.AreEqual(new DateTime(1990, 10, 10), reader.GetFieldValue<DateTime>(2));
                Assert.AreEqual(0, reader.GetFieldValue<int>(3));
            }
        }

        [Test]
        public void Upsert_Ignore1ToNRelation()
        {
            // create and insert entity with 1:n relation
            Teacher teacher = CreateTeacher();
            teacher.Classes = new List<Class>{CreateClass()};
            _context.Teachers.Upsert(teacher);

            string sqlQuery = "SELECT * FROM dbo.Class";
            SqlCommand command = new SqlCommand(sqlQuery, _connection);
            SqlDataReader reader = command.ExecuteReader();

            Assert.IsFalse(reader.HasRows);
        }

        [Test]
        public void Upsert_InsertEntityWithNToMRelation()
        {
            // create and insert entity with n:m relation
            Class c = CreateClass();
            _context.Classes.Upsert(c);

            string sqlQuery = "SELECT * FROM dbo.Student";
            SqlCommand command = new SqlCommand(sqlQuery, _connection);
            SqlDataReader reader = command.ExecuteReader();

            int id = 1;
            while (reader.Read())
            {
                Assert.AreEqual(id, reader.GetFieldValue<int>(0));
                Assert.AreEqual("Max Baum", reader.GetFieldValue<string>(1));
                Assert.AreEqual(new DateTime(1997, 01, 03), reader.GetFieldValue<DateTime>(2));
                Assert.AreEqual(0, reader.GetFieldValue<int>(3));
                id++;
            }

            sqlQuery = "SELECT * FROM dbo.StudentClass";
            command = new SqlCommand(sqlQuery, _connection);
            reader = command.ExecuteReader();

            id = 1;
            while (reader.Read())
            {
                Assert.AreEqual(1, reader.GetFieldValue<int>(0));
                Assert.AreEqual(id, reader.GetFieldValue<int>(1));
                id++;
            }
        }

        [Test]
        public void UpsertRange()
        {
            // create and insert entities
            Teacher teacher = CreateTeacher(1);
            Teacher teacher2 = CreateTeacher(2);
            _context.Teachers.UpsertRange(new List<Teacher>{teacher, teacher2});

            string sqlQuery = "SELECT * FROM dbo.Teacher";
            SqlCommand command = new SqlCommand(sqlQuery, _connection);
            SqlDataReader reader = command.ExecuteReader();

            int id = 1;
            while (reader.Read())
            {
                Assert.AreEqual(id, reader.GetFieldValue<int>(0));
                Assert.AreEqual("Tom Truthahn", reader.GetFieldValue<string>(1));
                Assert.AreEqual(new DateTime(1990, 10, 10), reader.GetFieldValue<DateTime>(2));
                Assert.AreEqual(0, reader.GetFieldValue<int>(3));
                id++;
            }
        }
    }
}