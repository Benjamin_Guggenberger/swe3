﻿using System;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using ORM.Helper;
using ORM.MetaData;

namespace ORM.Tests
{
    public class MetaDataTests : BaseTests
    {
        [Test]
        public void SetMetaDataInfo_PrimaryKey()
        {
            Class c = new Class();

            PropertyInfo property = c.GetType().GetProperty("ClassId");

            MetaDataInfoTable tableInfo = new MetaDataInfoTable();
            MetaDataInfoColumn columnInfo = new MetaDataInfoColumn();
            columnInfo.SetMetaDataInfo(property, tableInfo);

            Assert.IsTrue(columnInfo.IsPrimaryKey);
            Assert.AreEqual(property.Name, tableInfo.PrimaryKey);
            Assert.AreEqual(Constants.DataTypes.Integer, columnInfo.DataType);
        }

        [Test]
        public void SetMetaDataInfo_ForeignKey()
        {
            Class c = new Class();

            PropertyInfo property = c.GetType().GetProperty("Teacher");

            MetaDataInfoTable tableInfo = new MetaDataInfoTable();
            MetaDataInfoColumn columnInfo = new MetaDataInfoColumn();
            columnInfo.SetMetaDataInfo(property, tableInfo);

            Assert.IsTrue(columnInfo.IsForeignKey);
            Assert.AreEqual($"Fk_{property.PropertyType.Name}Id", columnInfo.ColumnName);
            Assert.AreEqual(property.PropertyType.Name, columnInfo.RelatedTableName);
            Assert.AreEqual(property.PropertyType, columnInfo.RelatedTableType);
            Assert.AreEqual(DeleteOption.None, columnInfo.DeleteOption);
            Assert.AreEqual(Constants.DataTypes.Integer, columnInfo.DataType);
        }

        [Test]
        public void SetMetaDataInfo_List1ToN()
        {
            Teacher teacher = new Teacher();

            PropertyInfo property = teacher.GetType().GetProperty("Classes");

            MetaDataInfoTable tableInfo = new MetaDataInfoTable();
            MetaDataInfoColumn columnInfo = new MetaDataInfoColumn();
            columnInfo.SetMetaDataInfo(property, tableInfo);

            Assert.IsTrue(columnInfo.IsList1ToN);
            Assert.AreEqual(property.PropertyType.GenericTypeArguments.First().Name, columnInfo.RelatedTableName);
            Assert.AreEqual(null, columnInfo.DataType);
        }

        [Test]
        public void SetMetaDataInfo_ListNToM()
        {
            Class c = new Class();

            PropertyInfo property = c.GetType().GetProperty("Students");

            MetaDataInfoTable tableInfo = new MetaDataInfoTable();
            MetaDataInfoColumn columnInfo = new MetaDataInfoColumn();
            columnInfo.SetMetaDataInfo(property, tableInfo);

            Type relatedTableType = property.PropertyType.GenericTypeArguments.First();

            Assert.IsTrue(columnInfo.IsListNToM);
            Assert.AreEqual(relatedTableType.Name, columnInfo.RelatedTableName);
            Assert.AreEqual(relatedTableType, columnInfo.RelatedTableType);
            Assert.AreEqual("StudentClass", columnInfo.JoinTableName);
            Assert.AreEqual(null, columnInfo.DataType);
        }

        [Test]
        public void SetMetaDataInfo_StringMaxLength_NotNull()
        {
            Course c = new Course();

            PropertyInfo property = c.GetType().GetProperty("Name");

            MetaDataInfoTable tableInfo = new MetaDataInfoTable();
            MetaDataInfoColumn columnInfo = new MetaDataInfoColumn();
            columnInfo.SetMetaDataInfo(property, tableInfo);

            Assert.IsTrue(columnInfo.NotNull);
            Assert.AreEqual("VARCHAR(16)", columnInfo.DataType);
        }
    }
}
