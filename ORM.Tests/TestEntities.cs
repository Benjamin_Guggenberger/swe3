﻿using System;
using System.Collections.Generic;
using ORM.Annotation;
using ORM.Helper;

namespace ORM.Tests
{
    public class Student
    {
        [PrimaryKey]
        public int StudentId { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public Sex Sex { get; set; }


        [ListNToM("StudentClass")]
        public List<Class> Classes { get; set; }
    }

    public class Class
    {
        [PrimaryKey]
        public int ClassId { get; set; }
        public string ClassName { get; set; }

        [ForeignKey]
        public Teacher Teacher { get; set; }

        [ListNToM("StudentClass")]
        public List<Student> Students { get; set; }
    }

    public class Teacher
    {
        [PrimaryKey]
        public int TeacherId { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public Sex Sex { get; set; }

        [List1ToN]
        public List<Class> Classes { get; set; }

        [List1ToN] 
        public List<Course> Courses { get; set; }
    }

    public class Course
    {
        [PrimaryKey]
        public int CourseId { get; set; }

        [StringMaxLength(16)]
        [NotNull]
        public string Name { get; set; }
        public bool IsBoring { get; set; }

        [ForeignKey(DeleteOption.SetNull)]
        public Teacher Teacher { get; set; }
    }

    public enum Sex
    {
        Male,
        Female,
        Other
    }
}