﻿using System.Data;
using ORM.Core;
using ORM.Core.Interface;

namespace ORM.Tests
{
    public class TestContext : Context
    {
        public TestContext(IDbConnection connection)
            : base(connection)
        {
            Teachers = Initialize<Teacher>();
            Students = Initialize<Student>();
            Courses = Initialize<Course>();
            Classes = Initialize<Class>();
        }

        public IEntity<Student> Students { get; set; }
        public IEntity<Class> Classes { get; set; }
        public IEntity<Teacher> Teachers { get; set; }
        public IEntity<Course> Courses { get; set; }
    }
}
