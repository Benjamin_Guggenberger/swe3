using System.Data;
using System.Data.SqlClient;
using NUnit.Framework;

namespace ORM.Tests
{
    public class ConfigurationTests : BaseTests
    {
        [Test]
        public void ConnectionState_IsOpen()
        {
            Assert.AreEqual(ConnectionState.Open, _connection.State);
        }

        [Test]
        public void Context_Initialize_InitializesEntities()
        {
            Assert.IsNotNull(_context.Classes);
            Assert.IsNotNull(_context.Courses);
            Assert.IsNotNull(_context.Teachers);
            Assert.IsNotNull(_context.Students);
        }

        [Test]
        public void Context_Initialize_CreatesTables()
        {
            string sqlQuery = "SELECT 1 FROM dbo.StudentClass;";
            sqlQuery += "SELECT 1 FROM dbo.Student;";
            sqlQuery += "SELECT 1 FROM dbo.Class;";
            sqlQuery += "SELECT 1 FROM dbo.Course;";
            sqlQuery += "SELECT 1 FROM dbo.Teacher;";
            SqlCommand command = new SqlCommand(sqlQuery, _connection);

            Assert.DoesNotThrow(() => command.ExecuteNonQuery());
        }
    }
}