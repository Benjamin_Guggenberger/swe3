﻿using System;
using System.Data.SqlClient;
using NUnit.Framework;

namespace ORM.Tests
{
    public class TransactionTests : BaseTests
    {
        [Test]
        public void HasTransactionIsTrue_AfterBeginTransaction()
        {
            _context.SqlTransactionProvider.BeginTransaction();

            Assert.IsTrue(_context.SqlTransactionProvider.HasTransaction);
        }

        [Test]
        public void HasTransactionIsFalse_AfterCommit()
        {
            _context.SqlTransactionProvider.BeginTransaction();
            _context.SqlTransactionProvider.Commit();

            Assert.IsFalse(_context.SqlTransactionProvider.HasTransaction);
        }

        [Test]
        public void HasTransactionIsFalse_AfterRollback()
        {
            _context.SqlTransactionProvider.BeginTransaction();
            _context.SqlTransactionProvider.Rollback();

            Assert.IsFalse(_context.SqlTransactionProvider.HasTransaction);
        }

        [Test]
        public void BeginTransaction_ThrowsException_WhenTransactionExist()
        {
            _context.SqlTransactionProvider.BeginTransaction();
            Assert.Throws<InvalidOperationException>(() => _context.SqlTransactionProvider.BeginTransaction());
        }

        [Test]
        public void Commit_ThrowsException_WhenNoTransactionExist()
        {
            Assert.Throws<InvalidOperationException>(() => _context.SqlTransactionProvider.Commit());
        }

        [Test]
        public void Rollback_ThrowsException_WhenNoTransactionExist()
        {
            Assert.Throws<InvalidOperationException>(() => _context.SqlTransactionProvider.Rollback());
        }
    }
}
