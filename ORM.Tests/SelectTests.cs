﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ORM.Helper;

namespace ORM.Tests
{
    public class SelectTests : BaseTests
    {
        [Test]
        public void LoadAll_IsEmpty()
        {
            IReadOnlyCollection<Teacher> teachers = _context.Teachers.LoadAll().GetObjects().ToList();
            Assert.IsEmpty(teachers);
        }

        [Test]
        public void LoadAll_ReturnsSingleEntity()
        {
            // create and insert entity
            Teacher teacher = CreateTeacher();
            _context.Teachers.Upsert(teacher);

            IReadOnlyCollection<Teacher> teachers = _context.Teachers.LoadAll().GetObjects().ToList();
            Assert.IsNotEmpty(teachers);
            Assert.AreEqual(1, teachers.Count);
        }

        [Test]
        public void LoadAll_ReturnsManyEntites()
        {
            // create and insert entities
            Teacher teacher = CreateTeacher();
            Teacher teacher2 = CreateTeacher(2);
            Teacher teacher3 = CreateTeacher(3);

            _context.Teachers.UpsertRange(new List<Teacher>{teacher, teacher2, teacher3});

            IReadOnlyCollection<Teacher> teachers = _context.Teachers.LoadAll().GetObjects().ToList();
            Assert.IsNotEmpty(teachers);
            Assert.AreEqual(3, teachers.Count);
        }

        [Test]
        public void LoadAllWhere_Equal()
        {
            // create and insert entities
            Teacher teacher = CreateTeacher();
            Teacher teacher2 = CreateTeacher(2);
            Teacher teacher3 = CreateTeacher(3);

            _context.Teachers.UpsertRange(new List<Teacher> { teacher, teacher2, teacher3 });

            List<Teacher> teachers = _context.Teachers.LoadAllWhere(nameof(Teacher.TeacherId), CompareOption.Equal, 1).GetObjects().ToList();
            Assert.IsNotEmpty(teachers);
            Assert.AreEqual(1, teachers.Count);
            Assert.AreEqual(1, teachers.First().TeacherId);
        }

        [Test]
        public void LoadAllWhere_Bigger()
        {
            // create and insert entities
            Teacher teacher = CreateTeacher();
            Teacher teacher2 = CreateTeacher(2);
            Teacher teacher3 = CreateTeacher(3);

            _context.Teachers.UpsertRange(new List<Teacher> { teacher, teacher2, teacher3 });

            List<Teacher> teachers = _context.Teachers.LoadAllWhere(nameof(Teacher.TeacherId), CompareOption.Bigger, 2).GetObjects().ToList();
            Assert.IsNotEmpty(teachers);
            Assert.AreEqual(1, teachers.Count);
            Assert.AreEqual(3, teachers.First().TeacherId);
        }

        [Test]
        public void LoadAllWhere_BiggerOrEqual()
        {
            // create and insert entities
            Teacher teacher = CreateTeacher();
            Teacher teacher2 = CreateTeacher(2);
            Teacher teacher3 = CreateTeacher(3);

            _context.Teachers.UpsertRange(new List<Teacher> { teacher, teacher2, teacher3 });

            List<Teacher> teachers = _context.Teachers.LoadAllWhere(nameof(Teacher.TeacherId), CompareOption.BiggerOrEqual, 3).GetObjects().ToList();
            Assert.IsNotEmpty(teachers);
            Assert.AreEqual(1, teachers.Count);
            Assert.AreEqual(3, teachers.First().TeacherId);
        }

        [Test]
        public void LoadAllWhere_Smaller()
        {
            // create and insert entities
            Teacher teacher = CreateTeacher();
            Teacher teacher2 = CreateTeacher(2);
            Teacher teacher3 = CreateTeacher(3);

            _context.Teachers.UpsertRange(new List<Teacher> { teacher, teacher2, teacher3 });

            List<Teacher> teachers = _context.Teachers.LoadAllWhere(nameof(Teacher.TeacherId), CompareOption.Smaller, 2).GetObjects().ToList();
            Assert.IsNotEmpty(teachers);
            Assert.AreEqual(1, teachers.Count);
            Assert.AreEqual(1, teachers.First().TeacherId);
        }

        [Test]
        public void LoadAllWhere_SmallerOrEqual()
        {
            // create and insert entities
            Teacher teacher = CreateTeacher();
            Teacher teacher2 = CreateTeacher(2);
            Teacher teacher3 = CreateTeacher(3);

            _context.Teachers.UpsertRange(new List<Teacher> { teacher, teacher2, teacher3 });

            List<Teacher> teachers = _context.Teachers.LoadAllWhere(nameof(Teacher.TeacherId), CompareOption.SmallerOrEqual, 1).GetObjects().ToList();
            Assert.IsNotEmpty(teachers);
            Assert.AreEqual(1, teachers.Count);
            Assert.AreEqual(1, teachers.First().TeacherId);
        }

        [Test]
        public void LoadByPrimaryKey_IsNull()
        {
            Teacher teacher = _context.Teachers.LoadByPrimaryKey(1);
            Assert.IsNull(teacher);
        }

        [Test]
        public void LoadByPrimaryKey_IsNotNull()
        {
            // create and insert entity
            Teacher t = CreateTeacher();
            _context.Teachers.Upsert(t);

            Teacher teacher = _context.Teachers.LoadByPrimaryKey(1);
            Assert.IsNotNull(teacher);
        }

        [Test]
        public void Load_ReadValues()
        {
            // create and insert entity
            Teacher t = CreateTeacher();
            _context.Teachers.Upsert(t);

            Teacher teacher = _context.Teachers.LoadByPrimaryKey(1);
            Assert.IsNotNull(teacher);
            Assert.AreEqual(1, teacher.TeacherId);
            Assert.AreEqual("Tom Truthahn", teacher.Name);
            Assert.AreEqual(new DateTime(1990, 10, 10), teacher.BirthDate);
            Assert.AreEqual(Sex.Male, teacher.Sex);
            Assert.IsNull(teacher.Classes);
            Assert.IsNull(teacher.Courses);
        }

        [Test]
        public void Load_1ToNRelationsAreNull()
        {
            // create and insert entity
            Class c = CreateClass();
            _context.Classes.Upsert(c);

            Teacher teacher = _context.Teachers.LoadByPrimaryKey(1);
            Assert.IsNotNull(teacher);
            Assert.AreEqual(1, teacher.TeacherId);
            Assert.AreEqual("Tom Truthahn", teacher.Name);
            Assert.AreEqual(new DateTime(1990, 10, 10), teacher.BirthDate);
            Assert.AreEqual(Sex.Male, teacher.Sex);
            Assert.IsNull(teacher.Classes);
            Assert.IsNull(teacher.Courses);
        }

        [Test]
        public void Load_GetValuesFor1ToNRelation()
        {
            // create and insert entity
            Class c = CreateClass();
            _context.Classes.Upsert(c);

            Teacher teacher = _context.Teachers.LoadByPrimaryKey(1);

            // in practice you would use: context.Classes.LoadAll().Where(c => c.Teacher?.TeacherId == 1) ...
            Class c2 = _context.Classes.LoadByPrimaryKey(1);
            
            Assert.AreEqual(teacher.TeacherId, c2.Teacher.TeacherId);
            // ... then you would do: teacher.Classes = new List<Class>{c2};
        }

        [Test]
        public void Load_NToMRelation()
        {
            // create and insert entity
            Class c = CreateClass();
            _context.Classes.Upsert(c);

            Class c2 = _context.Classes.LoadByPrimaryKey(1);
            List<Student> students = _context.Students.LoadAll().GetObjects().ToList();

            Assert.IsNotNull(c2);
            Assert.IsNotEmpty(students);
            Assert.AreEqual(2, students.Count);
        }

        [Test]
        public void LoadAll_Where()
        {
            // create and insert entities
            Teacher teacher = CreateTeacher();
            Teacher teacher2 = CreateTeacher(2);
            Teacher teacher3 = CreateTeacher(3);

            _context.Teachers.UpsertRange(new List<Teacher> {teacher, teacher2, teacher3});

            List<Teacher> teachers = _context.Teachers.LoadAll().Where(t => t.TeacherId > 2).GetObjects().ToList();

            Assert.IsNotEmpty(teachers);
            Assert.AreEqual(1, teachers.Count);
            Assert.Greater(teachers.First().TeacherId, 2);
        }

        [Test]
        public void LoadAll_OrderBy()
        {
            // create and insert entities
            Teacher teacher = CreateTeacher();
            Teacher teacher2 = CreateTeacher(2);
            Teacher teacher3 = CreateTeacher(3);

            _context.Teachers.UpsertRange(new List<Teacher> { teacher, teacher2, teacher3 });

            List<Teacher> teachers = _context.Teachers.LoadAll().OrderBy(t => t.TeacherId, false).GetObjects().ToList();

            Assert.IsNotEmpty(teachers);
            Assert.AreEqual(3, teachers.Count);
            Assert.AreEqual(3, teachers[0].TeacherId);
            Assert.AreEqual(2, teachers[1].TeacherId);
            Assert.AreEqual(1, teachers[2].TeacherId);
        }

        [Test]
        public void LoadAll_GetProperty()
        {
            // create and insert entities
            Teacher teacher = CreateTeacher(5);

            _context.Teachers.Upsert(teacher);

            List<int> ids = _context.Teachers.LoadAll().GetProperty(t => t.TeacherId).ToList();

            Assert.IsNotEmpty(ids);
            Assert.AreEqual(1, ids.Count);
            Assert.AreEqual(5, ids.First());
        }
    }
}
