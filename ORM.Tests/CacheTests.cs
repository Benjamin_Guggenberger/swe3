﻿using System;
using NUnit.Framework;
using ORM.Cache;
using ORM.Cache.Interface;
using ORM.MetaData;

namespace ORM.Tests
{
    public class CacheTests : BaseTests
    {
        [Test]
        public void CacheRegister_AddOrUpdate_Add()
        {
            Teacher teacher = CreateTeacher();
            Type type = teacher.GetType();

            ICacheRegister cacheRegister = new CacheRegister(new MetaDataRegister());
            cacheRegister.AddType(type);

            // insert
            cacheRegister.AddOrUpdate(type, teacher.TeacherId, teacher);

            cacheRegister.TryGetValue(type, teacher.TeacherId, out object obj);
            var newTeacher = (Teacher) obj;

            Assert.AreEqual(teacher.TeacherId, newTeacher.TeacherId);
            Assert.AreEqual(teacher.BirthDate, newTeacher.BirthDate);
            Assert.AreEqual(teacher.Name, newTeacher.Name);
            Assert.AreEqual(teacher.Sex, newTeacher.Sex);
        }

        [Test]
        public void CacheRegister_AddOrUpdate_Update()
        {
            Teacher teacher = CreateTeacher();
            Type type = teacher.GetType();

            ICacheRegister cacheRegister = new CacheRegister(new MetaDataRegister());
            cacheRegister.AddType(type);

            // insert
            cacheRegister.AddOrUpdate(type, teacher.TeacherId, teacher);

            // update
            teacher.Sex = Sex.Female;
            cacheRegister.AddOrUpdate(type, teacher.TeacherId, teacher);

            cacheRegister.TryGetValue(type, teacher.TeacherId, out object obj);
            var newTeacher = (Teacher)obj;

            Assert.AreEqual(teacher.TeacherId, newTeacher.TeacherId);
            Assert.AreEqual(teacher.BirthDate, newTeacher.BirthDate);
            Assert.AreEqual(teacher.Name, newTeacher.Name);
            Assert.AreEqual(teacher.Sex, newTeacher.Sex);
        }

        [Test]
        public void CacheRegister_Remove()
        {
            Teacher teacher = CreateTeacher();
            Type type = teacher.GetType();

            ICacheRegister cacheRegister = new CacheRegister(new MetaDataRegister());
            cacheRegister.AddType(type);

            // insert
            cacheRegister.AddOrUpdate(type, teacher.TeacherId, teacher);

            // remove
            cacheRegister.Remove(type, teacher.TeacherId, type.Name);

            Assert.IsFalse(cacheRegister.TryGetValue(type, teacher.TeacherId, out object _));
        }
    }
}
