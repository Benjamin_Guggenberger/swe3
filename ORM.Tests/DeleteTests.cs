﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using NUnit.Framework;

namespace ORM.Tests
{
    public class DeleteTests : BaseTests
    {
        [Test]
        public void Delete()
        {

            Teacher teacher = CreateTeacher();
            _context.Teachers.Upsert(teacher);
            List<Teacher> teachersBefore = _context.Teachers.LoadAll().GetObjects().ToList();
            _context.Teachers.Delete(teacher);
            List<Teacher> teachersNow = _context.Teachers.LoadAll().GetObjects().ToList();

            Assert.IsNotEmpty(teachersBefore);
            Assert.AreEqual(1, teachersBefore.Count);
            Assert.IsEmpty(teachersNow);
        }

        [Test]
        public void DeleteRange()
        {
            Teacher teacher = CreateTeacher();
            Teacher teacher2 = CreateTeacher(2);
            _context.Teachers.UpsertRange(new List<Teacher>{teacher, teacher2});

            List<Teacher> teachersBefore = _context.Teachers.LoadAll().GetObjects().ToList();
            _context.Teachers.DeleteRange(new List<Teacher>{teacher, teacher2});
            List<Teacher> teachersNow = _context.Teachers.LoadAll().GetObjects().ToList();

            Assert.IsNotEmpty(teachersBefore);
            Assert.AreEqual(2, teachersBefore.Count);
            Assert.IsEmpty(teachersNow);
        }

        [Test]
        public void Delete_ForeignKey_OnDeleteNoAction_ThrowsException()
        {
            Class c = CreateClass();
            _context.Classes.Upsert(c);

            Assert.Throws<SqlException>(() => _context.Teachers.Delete(c.Teacher));
        }

        [Test]
        public void Delete_ForeignKey_OnDeleteSetNull()
        {
            Course c = CreateCourse();
            _context.Courses.Upsert(c);

            List<Course> coursesBefore = _context.Courses.LoadAll().GetObjects().ToList();
            _context.Teachers.Delete(c.Teacher);
            List<Course> coursesNow = _context.Courses.LoadAll().GetObjects().ToList();

            Assert.IsNotEmpty(coursesBefore);
            Assert.AreEqual(1, coursesNow.Count);
            Assert.IsNotNull(coursesBefore.First().Teacher);
            Assert.IsNotEmpty(coursesNow);
            Assert.AreEqual(1, coursesNow.Count);
            Assert.IsNull(coursesNow.First().Teacher);
        }

        [Test]
        public void Delete_NToMRelation()
        {
            Class c = CreateClass();
            _context.Classes.Upsert(c);


            List<Class> classesBefore = _context.Classes.LoadAll().GetObjects().ToList();
            List<Student> studentsBefore = _context.Students.LoadAll().GetObjects().ToList();
            _context.Students.Delete(CreateStudent(1));
            List<Class> classesNow = _context.Classes.LoadAll().GetObjects().ToList();
            List<Student> studentsNow = _context.Students.LoadAll().GetObjects().ToList();

            Assert.IsNotEmpty(classesBefore);
            Assert.AreEqual(2, classesBefore.First().Students.Count);
            Assert.AreEqual(2, studentsBefore.Count);

            Assert.IsNotEmpty(classesBefore);
            Assert.AreEqual(1, classesNow.First().Students.Count);
            Assert.AreEqual(1, studentsNow.Count);
        }
    }
}
