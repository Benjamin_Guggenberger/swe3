using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using NUnit.Framework;
using ORM.DataAccess;

namespace ORM.Tests
{
    public class BaseTests
    {
        protected SqlConnection _connection;
        protected TestContext _context;

        [SetUp]
        protected void Setup()
        {
            string connectionString = "Data Source = .\\SQLEXPRESS; Initial Catalog = TestDB; Integrated Security = True; MultipleActiveResultSets = True";
            _connection = (SqlConnection)DbFactory.GetConnection(connectionString);
            _connection.Open();
            ResetDb();
            _context = CreateContext();
        }

        [TearDown]
        protected void TearDown()
        {
            _context.SqlTransactionProvider.Dispose();
            ResetDb();
            _connection.Close();
        }

        private void ResetDb()
        {
            string sqlQuery = "DROP TABLE IF EXISTS dbo.StudentClass;";
            sqlQuery += "DROP TABLE IF EXISTS dbo.Student;";
            sqlQuery += "DROP TABLE IF EXISTS dbo.Course;";
            sqlQuery += "DROP TABLE IF EXISTS dbo.Class;";
            sqlQuery += "DROP TABLE IF EXISTS dbo.Teacher;";
            SqlCommand command = new SqlCommand(sqlQuery, _connection);
            command.ExecuteNonQuery();
        }

        protected TestContext CreateContext()
        {
            return new TestContext(_connection);
        }

        protected Teacher CreateTeacher(int id = 1)
        {
            return new Teacher
            {
                TeacherId = id,
                BirthDate = new DateTime(1990, 10, 10),
                Sex = Sex.Male,
                Name = "Tom Truthahn"
            };
        }

        protected Course CreateCourse()
        {
            return new Course
            {
                CourseId = 1,
                Teacher = CreateTeacher(),
                IsBoring = true,
                Name = "Maths"
            };
        }

        protected Class CreateClass(int id = 1)
        {
            return new Class
            {
                ClassId = 1,
                Teacher = CreateTeacher(),
                ClassName = "AUDIMAX",
                Students = new List<Student>{CreateStudent(1), CreateStudent(2)}
            };
        }

        protected Student CreateStudent(int id)
        {
            return new Student
            {
                StudentId = id,
                BirthDate = new DateTime(1997, 01, 03),
                Name = "Max Baum",
                Sex = Sex.Male
            };
        }
    }
}