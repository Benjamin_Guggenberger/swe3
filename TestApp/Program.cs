﻿using System;
using System.Data.SqlClient;
using ORM.DataAccess;
using System.Configuration;
using System.Data;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateDb();

            string connectionString = ConfigurationManager.ConnectionStrings["TestDB"].ConnectionString;
            using (IDbConnection connection = DbFactory.GetConnection(connectionString))
            {
                connection.Open();

                ResetDb((SqlConnection)connection);

                var context = new DerivedContext(connection);

                // add teachers
                TestAppTeachers.AddTeacher(context);

                // load teachers
                TestAppTeachers.ViewTeachers(context);

                // update existing teacher
                TestAppTeachers.UpdateTeacher(context);

                // load teachers
                TestAppTeachers.ViewTeachers(context);

                // add courses (has Teacher as Foreign Key)
                TestAppCourses.AddCourse(context);

                // load courses
                TestAppCourses.ViewCourses(context);

                // delete teacher who is Foreign Key of a course
                TestAppTeachers.DeleteTeacher(context);

                // load teachers
                TestAppTeachers.ViewTeachers(context);

                // load courses
                TestAppCourses.ViewCourses(context);

                // add students (has n:m relation with classes)
                TestAppStudents.AddStudent(context);

                // add classes (has n:m relation with students)
                TestAppClasses.AddClass(context);

                // load students
                TestAppStudents.ViewStudents(context);

                // load classes
                TestAppClasses.ViewClasses(context);

                // delete class
                TestAppClasses.DeleteClass(context);

                // load students
                TestAppStudents.ViewStudents(context);

                // load classes
                TestAppClasses.ViewClasses(context);

                connection.Close();
            }
        }

        private static void CreateDb()
        {
            string cnStr = @"Data Source = .\SQLEXPRESS; Integrated Security = True";
            using (IDbConnection connection = DbFactory.GetConnection(cnStr))
            {
                connection.Open();
                DbFactory.CreateDb("TestDB", (SqlConnection)connection);
                connection.Close();
            }
        }

        private static void ResetDb(SqlConnection connection)
        {
            string sqlQuery = "DROP TABLE IF EXISTS dbo.StudentClass;";
            sqlQuery += "DROP TABLE IF EXISTS dbo.Student;";
            sqlQuery += "DROP TABLE IF EXISTS dbo.Course;";
            sqlQuery += "DROP TABLE IF EXISTS dbo.Class;";
            sqlQuery += "DROP TABLE IF EXISTS dbo.Teacher;";
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            command.ExecuteNonQuery();
        }
    }
}
