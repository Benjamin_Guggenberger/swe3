﻿using System;
using System.Collections.Generic;
using ORM.Annotation;
using ORM.Helper;

namespace TestApp
{
    public class Student
    {
        [PrimaryKey]
        public int StudentId { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public Sex Sex { get; set; }
        [ListNToM("StudentClass")]
        public ICollection<Class> Classes { get; set; }
    }

    public class Class
    {
        [PrimaryKey]
        public int ClassId { get; set; }
        public string ClassName { get; set; }

        [ForeignKey(DeleteOption.SetNull)]
        public Teacher Teacher { get; set; }

        [ListNToM("StudentClass")]
        public ICollection<Student> Students { get; set; }
    }

    public class Teacher
    {
        [PrimaryKey]
        public int TeacherId { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public Sex Sex { get; set; }
        [List1ToN]
        public ICollection<Class> Classes { get; set; }
        [List1ToN]
        public ICollection<Course> Courses { get; set; }
    }

    public class Course
    {
        [PrimaryKey]
        public int CourseId { get; set; }
        [StringMaxLength(16)]
        [NotNull]
        public string Name { get; set; }
        public bool IsBoring { get; set; }
        [ForeignKey(DeleteOption.SetNull)]
        public Teacher Teacher { get; set; }
    }

    public enum Sex
    {
        Male = 1,
        Female = 2,
        Other = 3
    }
}