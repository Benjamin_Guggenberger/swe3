﻿using System.Data;
using ORM.Core;
using ORM.Core.Interface;

namespace TestApp
{
    public class DerivedContext : Context
    {
        public DerivedContext(IDbConnection connection) 
            : base(connection)
        {
            Teachers = Initialize<Teacher>();
            Classes = Initialize<Class>();
            Students = Initialize<Student>();
            Courses = Initialize<Course>();
        }

        public IEntity<Student> Students { get; }
        public IEntity<Class> Classes { get;  }
        public IEntity<Teacher> Teachers { get;  }
        public IEntity<Course> Courses { get; }
    }
}