﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestApp
{
    public static class TestAppCourses
    {
        public static void AddCourse(DerivedContext context)
        {
            while (true)
            {
                int courseId;
                bool isBoring;
                Teacher teacher;

                Console.WriteLine("Create a Course:");
                while (true)
                {
                    Console.Write("CourseId(int): ");
                    if (int.TryParse(Console.ReadLine(), out courseId))
                    {
                        Course c = context.Courses.LoadByPrimaryKey(courseId);
                        if (c == null)
                        {
                            break;
                        }
                        Console.WriteLine($"Course with '{courseId}' already exists.");
                        continue;
                    }

                    Console.WriteLine("Please enter a numeric value.");
                }

                Console.Write("Name: ");
                string name = Console.ReadLine();
                while (true)
                {
                    Console.Write("Is Boring(Y/N): ");
                    string input = Console.ReadLine()?.ToUpper();
                    if (input == "Y")
                    {
                        isBoring = true;
                        break;
                    }
                    if (input == "N")
                    {
                        isBoring = false;
                        break;
                    }
                    Console.WriteLine("Invalid input: Please enter 'Y' or 'N'.");
                }

                while (true)
                {
                    Console.Write("Id of the courses' teacher: ");
                    if (int.TryParse(Console.ReadLine(), out int teacherId))
                    {
                        teacher = context.Teachers.LoadByPrimaryKey(teacherId);
                        if (teacher != null)
                        {
                            break;
                        }

                        Console.WriteLine($"No Teacher found with TeacherId '{teacherId}'.");
                        continue;
                    }

                    Console.WriteLine("Please enter a numeric value.");
                }

                Course course = new Course
                {
                    CourseId = courseId,
                    Name = name,
                    IsBoring = isBoring,
                    Teacher = teacher
                };

                Console.WriteLine("Saving Course to the data base...");
                context.Courses.Upsert(course);
                Console.WriteLine("Successfully saved Course to the data base!\n");

                while (true)
                {
                    Console.Write("Do you want to add another course? (Y/N): ");
                    string input = Console.ReadLine()?.ToUpper();
                    if (input == "Y")
                    {
                        break;
                    }
                    if (input == "N")
                    {
                        return;
                    }
                    Console.WriteLine("Invalid input: Please enter 'Y' or 'N'.");
                }
            }
        }

        public static void ViewCourses(DerivedContext context)
        {
            List<Course> courses = context.Courses.LoadAll().GetObjects().ToList();
            if (courses.Count == 0)
            {
                return;
            }

            Console.Write("\n--------------------Courses-----------------------");
            foreach (Course course in courses)
            {
                Console.WriteLine($"\nCourseId: {course.CourseId}");
                Console.WriteLine($"Name: {course.Name}");
                Console.WriteLine($"IsBoring: {course.IsBoring}");
                Console.WriteLine($"Teacher: {course.Teacher?.TeacherId}");
            }

            Console.WriteLine("---------------------------------------------------\n");
        }
    }
}
