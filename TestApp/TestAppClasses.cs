﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestApp
{
    public static class TestAppClasses
    {
        public static void AddClass(DerivedContext context)
        {
            while (true)
            {
                int classId;
                Console.WriteLine("Create a Class:");
                while (true)
                {
                    Console.Write("ClassId(int): ");
                    if (int.TryParse(Console.ReadLine(), out classId))
                    {
                        Class cl = context.Classes.LoadByPrimaryKey(classId);
                        if (cl == null)
                        {
                            break;
                        }
                        Console.WriteLine($"Class with '{classId}' already exists.");
                        continue;
                    }

                    Console.WriteLine("Please enter a numeric value.");
                }

                Console.Write("ClassName: ");
                string className = Console.ReadLine();

                var students = new List<Student>();
                while (true)
                {
                    while (true)
                    {
                        Console.Write("Id of a student who is in the class: ");
                        if (int.TryParse(Console.ReadLine(), out int studentId))
                        {
                            Student student = context.Students.LoadByPrimaryKey(studentId);
                            if (student != null)
                            {
                                if (!students.Select(s => s.StudentId).Contains(studentId))
                                {
                                    students.Add(student);
                                }
                                break;
                            }

                            Console.WriteLine($"No Student found with StudentId '{studentId}'.");
                            continue;
                        }

                        Console.WriteLine("Please enter a numeric value.");
                    }
                    
                    if (!AddStudentForClass())
                    {
                        break;
                    }
                }

                Class c = new Class
                {
                    ClassId = classId,
                    ClassName = className,
                    Students = students
                };

                Console.WriteLine("Saving Class to the data base...");
                context.Classes.Upsert(c);
                Console.WriteLine("Successfully saved Class to the data base!\n");

                while (true)
                {
                    Console.Write("Do you want to add another Class? (Y/N): ");
                    string input = Console.ReadLine()?.ToUpper();
                    if (input == "Y")
                    {
                        break;
                    }
                    if (input == "N")
                    {
                        return;
                    }
                    Console.WriteLine("Invalid input: Please enter 'Y' or 'N'.");
                }
            }
        }

        private static bool AddStudentForClass()
        {
            while (true)
            {
                Console.Write("Do you want to add another Student for the Class? (Y/N): ");
                string input = Console.ReadLine()?.ToUpper();
                if (input == "Y")
                {
                    return true;
                }

                if (input == "N")
                {
                    return false;
                }

                Console.WriteLine("Invalid input: Please enter 'Y' or 'N'.");
            }
        }

        public static void DeleteClass(DerivedContext context)
        {
            while (true)
            {
                Console.Write("ClassId(s) in the data base: ");
                List<int> classIds = context.Classes.LoadAll().GetProperty(s => s.ClassId).ToList();
                foreach (int classId in classIds)
                {
                    Console.Write($"{classId} ");
                }

                Console.Write("\nEnter one of these ClassIds to remove Class: ");
                if (int.TryParse(Console.ReadLine(), out int id))
                {
                    if (classIds.Contains(id))
                    {
                        context.Classes.Delete(new Class { ClassId = id });
                        return;
                    }

                    Console.WriteLine($"Entered Id '{id}' is invalid.");
                    continue;
                }

                Console.WriteLine("Please enter a numeric value.");
            }
        }

        public static void ViewClasses(DerivedContext context)
        {
            List<Class> classes = context.Classes.LoadAll().GetObjects().ToList();
            if (classes.Count == 0)
            {
                return;
            }

            Console.Write("\n--------------------Classes-----------------------");
            foreach (Class c in classes)
            {
                Console.WriteLine($"\nClassId: {c.ClassId}");
                Console.WriteLine($"ClassName: {c.ClassName}");
                Console.WriteLine($"TeacherId: {c.Teacher?.TeacherId}");
                if (c.Students != null)
                {
                    Console.Write("StudentId(s): ");
                    foreach (Student student in c.Students)
                    {
                        Console.Write($"{student.StudentId} ");
                    }
                    Console.WriteLine();
                }
            }

            Console.WriteLine("---------------------------------------------------\n");
        }
    }
}
