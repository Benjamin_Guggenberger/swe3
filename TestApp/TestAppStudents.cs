﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestApp
{
    public static class TestAppStudents
    {
        public static void AddStudent(DerivedContext context)
        {
            while (true)
            {
                int studentId;
                DateTime birthDate;
                Sex sex;
                Console.WriteLine("Create a Student:");
                while (true)
                {
                    Console.Write("StudentId(int): ");
                    if (int.TryParse(Console.ReadLine(), out studentId))
                    {
                        Student s = context.Students.LoadByPrimaryKey(studentId);
                        if (s == null)
                        {
                            break;
                        }
                        Console.WriteLine($"Student with '{studentId}' already exists.");
                        continue;
                    }

                    Console.WriteLine("Please enter a numeric value.");
                }

                Console.Write("Name: ");
                string name = Console.ReadLine();
                while (true)
                {
                    Console.Write("Date of birth(yyyy-mm-dd): ");
                    if (DateTime.TryParse(Console.ReadLine(), out birthDate))
                    {
                        break;
                    }

                    Console.WriteLine("Please enter a valid date.");
                }

                while (true)
                {
                    Console.Write("Sex(1=Male,2=Female,3=Other): ");

                    if (int.TryParse(Console.ReadLine(), out int enumVal))
                    {
                        if (enumVal >= 1 && enumVal <= 3)
                        {
                            sex = (Sex)enumVal;
                            break;
                        }
                    }

                    Console.WriteLine("Please enter 1, 2 or 3.");
                }

                Student student = new Student
                {
                    StudentId = studentId,
                    BirthDate = birthDate,
                    Name = name,
                    Sex = sex
                };

                Console.WriteLine("Saving Student to the data base...");
                context.Students.Upsert(student);
                Console.WriteLine("Successfully saved Student to the data base!\n");

                while (true)
                {
                    Console.Write("Do you want to add another Student? (Y/N): ");
                    string input = Console.ReadLine()?.ToUpper();
                    if (input == "Y")
                    {
                        break;
                    }
                    if (input == "N")
                    {
                        return;
                    }
                    Console.WriteLine("Invalid input: Please enter 'Y' or 'N'.");
                }
            }
        }

        public static void ViewStudents(DerivedContext context)
        {
            List<Student> students = context.Students.LoadAll().GetObjects().ToList();
            if (students.Count == 0)
            {
                return;
            }

            Console.Write("\n--------------------Students-----------------------");
            foreach (Student student in students)
            {
                Console.WriteLine($"\nStudentId: {student.StudentId}");
                Console.WriteLine($"Name: {student.Name}");
                Console.WriteLine($"Date of birth: {student.BirthDate:MM/dd/yyyy}");
                Console.WriteLine($"Sex: {student.Sex}");
                if (student.Classes != null)
                {
                    Console.Write("ClassId(s): ");
                    foreach (Class c in student.Classes)
                    {
                        Console.Write($"{c.ClassId} ");
                    }

                    Console.WriteLine();
                }
            }
            Console.WriteLine("---------------------------------------------------\n");
        }
    }
}
