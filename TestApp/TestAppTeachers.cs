﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestApp
{
    public static class TestAppTeachers
    {
        public static void AddTeacher(DerivedContext context)
        {
            while (true)
            {
                int teacherId;
                DateTime birthDate;
                Sex sex;
                Console.WriteLine("Create a Teacher:");
                while (true)
                {
                    Console.Write("TeacherId(int): ");
                    if (int.TryParse(Console.ReadLine(), out teacherId))
                    {
                        Teacher t = context.Teachers.LoadByPrimaryKey(teacherId);
                        if (t == null)
                        {
                            break;
                        }
                        Console.WriteLine($"Teacher with '{teacherId}' already exists.");
                        continue;
                    }

                    Console.WriteLine("Please enter a numeric value.");
                }

                Console.Write("Name: ");
                string name = Console.ReadLine();
                while (true)
                {
                    Console.Write("Date of birth(yyyy-mm-dd): ");
                    if (DateTime.TryParse(Console.ReadLine(), out birthDate))
                    {
                        break;
                    }

                    Console.WriteLine("Please enter a valid date.");
                }

                while (true)
                {
                    Console.Write("Sex(1=Male,2=Female,3=Other): ");

                    if (int.TryParse(Console.ReadLine(), out int enumVal))
                    {
                        if (enumVal >= 1 && enumVal <= 3)
                        {
                            sex = (Sex)enumVal;
                            break;
                        }
                    }

                    Console.WriteLine("Please enter 1, 2 or 3.");
                }

                Teacher teacher = new Teacher
                {
                    TeacherId = teacherId,
                    BirthDate = birthDate,
                    Name = name,
                    Sex = sex
                };

                Console.WriteLine("Saving Teacher to the data base...");
                context.Teachers.Upsert(teacher);
                Console.WriteLine("Successfully saved Teacher to the data base!\n");

                while (true)
                {
                    Console.Write("Do you want to add another teacher? (Y/N): ");
                    string input = Console.ReadLine()?.ToUpper();
                    if (input == "Y")
                    {
                        break;
                    }
                    if (input == "N")
                    {
                        return;
                    }
                    Console.WriteLine("Invalid input: Please enter 'Y' or 'N'.");
                }
            }
        }

        public static void UpdateTeacher(DerivedContext context)
        {
            int teacherId;
            DateTime birthDate;
            Sex sex;
            while (true)
            {
                Console.Write("Update Teacher with TeacherId: ");
                if (int.TryParse(Console.ReadLine(), out teacherId))
                {
                    Teacher t = context.Teachers.LoadByPrimaryKey(teacherId);
                    if (t != null)
                    {
                        teacherId = t.TeacherId;
                        break;
                    }

                    Console.WriteLine($"No Teacher found with TeacherId '{teacherId}'.");
                    continue;
                }
                Console.WriteLine("Please enter a numeric value.");
            }

            Console.Write("Name: ");
            string name = Console.ReadLine();
            while (true)
            {
                Console.Write("Date of birth(yyyy-mm-dd): ");
                if (DateTime.TryParse(Console.ReadLine(), out birthDate))
                {
                    break;
                }

                Console.WriteLine("Please enter a valid date.");
            }

            while (true)
            {
                Console.Write("Sex(1=Male,2=Female,3=Other): ");

                if (int.TryParse(Console.ReadLine(), out int enumVal))
                {
                    if (enumVal >= 1 && enumVal <= 3)
                    {
                        sex = (Sex)enumVal;
                        break;
                    }
                }

                Console.WriteLine("Please enter 1, 2 or 3.");
            }

            Teacher teacher = new Teacher
            {
                TeacherId = teacherId,
                BirthDate = birthDate,
                Name = name,
                Sex = sex
            };

            Console.WriteLine("Updating Teacher in the data base...");
            context.Teachers.Upsert(teacher);
            Console.WriteLine("Successfully updated Teacher!\n");
        }

        public static void DeleteTeacher(DerivedContext context)
        {
            while (true)
            {
                Console.Write("TeacherId(s) who are the Foreign Key of an existing Course: ");
                List<int> teacherIds = context.Courses.LoadAll().GetProperty(c => c.Teacher.TeacherId).Distinct().ToList();
                foreach (int teacherId in teacherIds)
                {
                    Console.Write($"{teacherId} ");
                }

                Console.Write("\nEnter one of these TeacherIds to remove Teacher: ");
                if (int.TryParse(Console.ReadLine(), out int id))
                {
                    if (teacherIds.Contains(id))
                    {
                        context.Teachers.Delete(new Teacher { TeacherId = id });
                        return;
                    }

                    Console.WriteLine($"Entered Id '{id}' is invalid.");
                    continue;
                }

                Console.WriteLine("Please enter a numeric value.");
            }
        }

        public static void ViewTeachers(DerivedContext context)
        {
            List<Teacher> teachers = context.Teachers.LoadAll().GetObjects().ToList();
            if (teachers.Count == 0)
            {
                return;
            }

            Console.Write("\n--------------------Teachers-----------------------");
            foreach (Teacher teacher in teachers)
            {
                Console.WriteLine($"\nTeacherId: {teacher.TeacherId}");
                Console.WriteLine($"Name: {teacher.Name}");
                Console.WriteLine($"Date of birth: {teacher.BirthDate:MM/dd/yyyy}");
                Console.WriteLine($"Sex: {teacher.Sex}");
            }

            Console.WriteLine("---------------------------------------------------\n");
        }
    }
}
