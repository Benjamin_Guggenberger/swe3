﻿namespace ORM.Helper
{
    public enum CompareOption
    {
        Equal,
        Bigger,
        BiggerOrEqual,
        Smaller,
        SmallerOrEqual
    }
}
