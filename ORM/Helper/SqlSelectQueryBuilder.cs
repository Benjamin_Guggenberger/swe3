﻿namespace ORM.Helper
{
    /// <summary>
    /// Represents a class that creates sql select queries.
    /// </summary>
    public class SqlSelectQueryBuilder : SqlQueryBuilderBase
    {
        public void AppendSelectAll(string tableName)
        {
            _query = $"SELECT * FROM {tableName} ";
        }

        public void AppendSelectByPrimaryKey(string tableName, string primaryKeyColumnName, int primaryKeyValue)
        {
            AppendSelectAll(tableName);
            _query += $"WHERE {primaryKeyColumnName} = {primaryKeyValue}"; 
        }

        public void AppendSelectJoinTable(string joinTableName, string columnName, int primaryKeyValue)
        {
            AppendSelectAll(joinTableName);
            _query += $"WHERE Fk_{columnName}Id = {primaryKeyValue}";
        }

        public void AppendSelectWhere(string tableName, string name, string compareOption, double compareValue)
        {
            AppendSelectAll(tableName);
            _query += $"WHERE {name} {compareOption} {compareValue}";
        }
    }
}
