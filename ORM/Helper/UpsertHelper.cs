﻿using System.Collections.Generic;

namespace ORM.Helper
{
    public class UpsertHelper
    {
        /// <summary>
        /// Dictionary with column name and value for prepared statements.
        /// </summary>
        public IDictionary<string, object> ValueByColumnNameDictionary = new Dictionary<string, object>();

        /// <summary>
        /// The data base table name.
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// The primary key of the table.
        /// </summary>
        public string PrimaryKey { get; set; }
    }
}
