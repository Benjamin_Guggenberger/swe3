﻿namespace ORM.Helper
{
    /// <summary>
    /// Represents the base class that creates sql queries.
    /// </summary>
    public abstract class SqlQueryBuilderBase
    {
        protected string _query;

        public virtual string GetSqlQuery() => _query;

        protected void ReplaceCommaWithClosingBrackets()
        {
            if (!string.IsNullOrEmpty(_query))
            {
                _query = _query.Remove(_query.Length - 1, 1) + ")";
            }
        }
    }
}
