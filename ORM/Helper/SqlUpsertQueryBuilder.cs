﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ORM.Helper
{
    /// <summary>
    /// Represents a class that creates upsert (insert or update) sql queries.
    /// </summary>
    public class SqlUpsertQueryBuilder : SqlQueryBuilderBase
    {
        private UpsertHelper _helper;

        public SqlUpsertQueryBuilder()
        {
        }

        public SqlUpsertQueryBuilder(UpsertHelper helper)
        {
            _helper = helper;
        }

        /// <summary>
        /// Gets the upsert sql query string.
        /// </summary>
        /// <returns></returns>
        public override string GetSqlQuery()
        {
            ICollection<string> columnNames = _helper.ValueByColumnNameDictionary.Keys;

            if (columnNames.Count == 0)
            {
                throw new InvalidOperationException("No values set.");
            }

            _query = $"IF NOT EXISTS(SELECT * FROM {_helper.TableName} WHERE ";
            _query += $"{_helper.PrimaryKey} = @{_helper.PrimaryKey} AND ";
            _query = _query.Remove(_query.Length - 4) + ")";
            _query += $" INSERT INTO {_helper.TableName} (";
            foreach (string column in columnNames)
            {
                _query += $"{column},";
            }
            _query = _query.Remove(_query.Length - 1) + ")";
            _query += " VALUES (";
            foreach (string column in columnNames)
            {
                _query += $"@{column},";
            }
            _query = _query.Remove(_query.Length - 1) + ")";
            _query += $" ELSE UPDATE {_helper.TableName} SET ";
            foreach (string column in columnNames)
            {
                if (_helper.PrimaryKey != column)
                {
                    _query += $"{column} = @{column},";
                }
            }
            _query = _query.Remove(_query.Length - 1);
            _query += " WHERE ";
            _query += $"{_helper.PrimaryKey} = @{_helper.PrimaryKey}";

            return _query;
        }

        public string GetSqlQueryForJoinTable(string joinTableName, KeyValuePair<string, object> primaryKey1WithValue, KeyValuePair<string, object> primaryKey2WithValue)
        {
            _query = $"IF NOT EXISTS(SELECT * FROM {joinTableName} WHERE ";
            _query += $"FK_{primaryKey1WithValue.Key} = {primaryKey1WithValue.Value} AND Fk_{primaryKey2WithValue.Key} = {primaryKey2WithValue.Value}) ";
            _query += $"INSERT INTO {joinTableName} (Fk_{primaryKey1WithValue.Key}, Fk_{primaryKey2WithValue.Key}) ";
            _query += $"VALUES ({primaryKey1WithValue.Value}, {primaryKey2WithValue.Value})";
            return _query;
        }
    }
}