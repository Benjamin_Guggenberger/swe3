﻿namespace ORM.Helper
{
    /// <summary>
    /// Represents a static class with constants.
    /// </summary>
    public static class Constants
    {
        public static class DataTypes
        {
            public const string Integer = "INT";
            public const string Double = "DECIMAL";
            public const string Char = "CHAR";
            public const string StringDefaultMaxLength = "VARCHAR(64)";
            public const string Boolean = "BIT";
            public const string DateTime = "DATETIME";
        }

        public static class Attributes
        {
            public const string PrimaryKey = "PrimaryKeyAttribute";
            public const string ForeignKey = "ForeignKeyAttribute";
            public const string List1ToN = "List1ToNAttribute";
            public const string ListNToM = "ListNToMAttribute";
            public const string StringMaxLength = "StringMaxLengthAttribute";
            public const string NotNull = "NotNullAttribute";
        }
    }
}
