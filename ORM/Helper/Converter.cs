﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ORM.Helper
{
    /// <summary>
    /// Represents a class for conversion.
    /// </summary>
    public static class Converter
    {
        private static readonly IDictionary<Type, string> DataTypesDictionary = new Dictionary<Type, string>
        {
            [typeof(int)] = Constants.DataTypes.Integer,
            [typeof(long)] = Constants.DataTypes.Integer,
            [typeof(short)] = Constants.DataTypes.Integer,
            [typeof(Enum)] = Constants.DataTypes.Integer,
            [typeof(double)] = Constants.DataTypes.Double,
            [typeof(float)] = Constants.DataTypes.Double,
            [typeof(decimal)] = Constants.DataTypes.Double,
            [typeof(bool)] = Constants.DataTypes.Boolean,
            [typeof(string)] = Constants.DataTypes.StringDefaultMaxLength,
            [typeof(char)] = Constants.DataTypes.Char,
            [typeof(DateTime)] = Constants.DataTypes.DateTime,
        };

        /// <summary>
        /// Tries to map the c# data type to the data base data type.
        /// </summary>
        /// <param name="type">The c# data type.</param>
        /// <returns>True if the data base data type was found.</returns>
        public static string GetDbDataType(Type type)
        {
            // List1ToN and ListNToM
            if (type.IsGenericType && typeof(IEnumerable).IsAssignableFrom(type.GetGenericTypeDefinition()))
            {
                return null;
            }

            if (!DataTypesDictionary.TryGetValue(type, out string dataType))
            {
                // ForeignKeys
                return Constants.DataTypes.Integer;
            }

            return dataType;
        }

        /// <summary>
        /// Gets the string to a delete option for a foreign key.
        /// </summary>
        /// <param name="deleteOption">The delete option.</param>
        /// <returns>The string of the delete option.</returns>
        public static string GetDeleteOptionString(DeleteOption deleteOption)
        {
            switch (deleteOption)
            {
                case DeleteOption.None:
                    return "NO ACTION";
                case DeleteOption.Cascade:
                    return "CASCADE";
                case DeleteOption.SetNull:
                    return "SET NULL";
                default:
                    throw new InvalidOperationException("Unknown delete option.");
            }
        }

        public static string GetCompareOptionString(CompareOption compareOption)
        {
            switch (compareOption)
            {
                case CompareOption.Equal:
                    return "=";
                case CompareOption.Bigger:
                    return ">";
                case CompareOption.BiggerOrEqual:
                    return ">=";
                case CompareOption.Smaller:
                    return "<";
                case CompareOption.SmallerOrEqual:
                    return "<=";
                default:
                    throw new InvalidOperationException("Unknown filter option.");
            }
        }
    }
}
