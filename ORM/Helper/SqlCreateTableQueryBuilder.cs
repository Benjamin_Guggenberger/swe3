﻿using ORM.MetaData;
using System.Collections.Generic;

namespace ORM.Helper
{
    /// <summary>
    /// Represents a class that creates that creates sql queries for table creation.
    /// </summary>
    public class SqlCreateTableQueryBuilder : SqlQueryBuilderBase
    {
        public void AppendTableExistenceCheck(string tableName)
        {
            _query = $"IF NOT EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = '{tableName}' AND XTYPE = 'U') CREATE TABLE {tableName} (";
        }

        public void AppendCreateTable(MetaDataInfoColumn columnInfo)
        {
            _query += string.Concat(columnInfo.ColumnName, " ", columnInfo.DataType);

            if (columnInfo.RelatedTableName != null)
            {
                AppendForeignKey(columnInfo.RelatedTableName);
                AppendOnDelete(columnInfo.DeleteOption);
            }

            if (columnInfo.NotNull)
            {
                _query += "NOT NULL";
            }

            _query += ",";
        }

        public void AppendCreateJoinTable(string columnName, string relatedTableName)
        {
            _query += string.Concat(columnName, " ", Constants.DataTypes.Integer);

            AppendForeignKey(relatedTableName);
            AppendOnDelete(DeleteOption.Cascade);
            _query += ",";
        }

        private void AppendOnDelete(DeleteOption deleteOption)
        {
            _query += $"ON DELETE {Converter.GetDeleteOptionString(deleteOption)}";
        }

        private void AppendForeignKey(string relatedTable)
        {
            _query += $" FOREIGN KEY REFERENCES {relatedTable} ({relatedTable}Id)";
        }

        public void AppendPrimaryKeys(ICollection<string> primaryKeys)
        {
            _query += "PRIMARY KEY(";
            foreach (string primaryKey in primaryKeys)
            {
                _query += $"{primaryKey},";
            }
            ReplaceCommaWithClosingBrackets();
            _query += ")";
        }
    }
}
