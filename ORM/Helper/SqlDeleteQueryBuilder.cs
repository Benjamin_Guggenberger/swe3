﻿namespace ORM.Helper
{
    /// <summary>
    /// Represents a class that creates delete sql queries.
    /// </summary>
    public class SqlDeleteQueryBuilder : SqlQueryBuilderBase
    {
        public void AppendDeleteFrom(string tableName, string primaryKey)
        {
            _query = $"DELETE FROM {tableName} WHERE {primaryKey} = @{primaryKey}";
        }
    }
}