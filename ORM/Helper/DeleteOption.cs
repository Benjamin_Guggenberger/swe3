﻿namespace ORM.Helper
{
    public enum DeleteOption
    {
        None,
        Cascade,
        SetNull
    }
}
