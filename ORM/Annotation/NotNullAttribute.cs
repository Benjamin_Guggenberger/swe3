﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Annotation
{
    /// <summary>
    /// Marks a not null attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NotNullAttribute : Attribute
    {
    }
}
