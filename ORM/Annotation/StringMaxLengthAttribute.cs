﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Annotation
{
    /// <summary>
    /// Marks a foreign length attribute for strings.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class StringMaxLengthAttribute : Attribute
    {

        public StringMaxLengthAttribute(int stringMaxLength)
        {
        }
    }
}
