﻿using System;

namespace ORM.Annotation
{
    /// <summary>
    /// Marks an attribute with a one to many relation. Values can only be selected, not inserted or updated.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class List1ToNAttribute : Attribute
    {
        
    }
}
