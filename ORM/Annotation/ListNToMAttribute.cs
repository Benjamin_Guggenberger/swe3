﻿using System;

namespace ORM.Annotation
{
    /// <summary>
    /// Marks an attribute with a many to many relation.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ListNToMAttribute : Attribute
    {
        public ListNToMAttribute(string joinTableName)
        {
        }
    }
}
