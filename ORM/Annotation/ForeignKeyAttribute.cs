﻿using System;
using ORM.Helper;

namespace ORM.Annotation
{
    /// <summary>
    /// Marks a foreign key attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ForeignKeyAttribute : Attribute
    {
        public ForeignKeyAttribute(DeleteOption deleteOption = DeleteOption.None)
        {
        }
    }
}
