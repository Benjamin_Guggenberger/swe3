﻿using System;

namespace ORM.Annotation
{
    /// <summary>
    /// Marks a primary key attribute. Property must end with "Id".
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]

    public class PrimaryKeyAttribute : Attribute
    {
        public PrimaryKeyAttribute()
        {
        }
    }
}
