﻿using System.Collections.Generic;
using ORM.Helper;

namespace ORM.Core.Interface
{
    public interface IEntity<T>
        where T : class, new()
    {
        public T Upsert(T obj);
        public ICollection<T> UpsertRange(IEnumerable<T> objects);
        public void Delete(T obj);
        public void DeleteRange(IEnumerable<T> objects);
        public SelectApi<T> LoadAll();
        public SelectApi<T> LoadAllWhere(string propertyName, CompareOption compareOption, double compareValue);
        public T LoadByPrimaryKey(int primaryKeyValue);
    }
}