﻿using System.Collections.Generic;
using System.Linq;
using ORM.Cache.Interface;
using ORM.Core.Interface;
using ORM.DataAccess.Interface;
using ORM.Helper;
using ORM.MetaData.Interface;

namespace ORM.Core
{
    /// <summary>
    /// Represents a class that ca be used to perform create, read, update and delete operations
    /// </summary>
    /// <typeparam name="T">The c# class.</typeparam>
    public class Entity<T> : IEntity<T>
        where T : class, new()
    {
        private readonly UpsertManager<T> _upsertManager;
        private readonly DeleteManager<T> _deleteManager;
        private readonly SelectManager<T> _selectManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity{TSource}"/> class.
        /// </summary>
        /// <param name="metaDataRegister">The meta data register.</param>
        /// <param name="cacheRegister">The cache register.</param>
        /// <param name="queryExecutor">The sql query executor.</param>
        public Entity(IMetaDataRegister metaDataRegister, ICacheRegister cacheRegister, IQueryExecutor queryExecutor)
        {
            var createManager = new CreateManager<T>(metaDataRegister, queryExecutor);
            createManager.CreateTable();
            cacheRegister.AddType(typeof(T));

            _selectManager = new SelectManager<T>(metaDataRegister, cacheRegister, queryExecutor);
            _upsertManager = new UpsertManager<T>(metaDataRegister, cacheRegister, queryExecutor);
            _deleteManager = new DeleteManager<T>(metaDataRegister, cacheRegister, queryExecutor);
        }

        /// <summary>
        /// Inserts or updates an object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The new object.</returns>
        public T Upsert(T obj)
        {
            return (T)_upsertManager.Upsert(obj);
        }

        /// <summary>
        /// Inserts or updated a container with objects.
        /// </summary>
        /// <param name="objects">The objects.</param>
        /// <returns>The objects.</returns>
        public ICollection<T> UpsertRange(IEnumerable<T> objects)
        {
            var retVal = new List<T>();
            foreach (T obj in objects)
            {
                retVal.Add(Upsert(obj));
            }

            return retVal;
        }

        /// <summary>
        /// Deletes an object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void Delete(T obj)
        {
            _deleteManager.Delete(obj);
        }

        /// <summary>
        /// Deletes a container with objects.
        /// </summary>
        /// <param name="objects">The objects.</param>
        public void DeleteRange(IEnumerable<T> objects)
        {
            foreach (T obj in objects)
            {
                Delete(obj);
            }
        }

        /// <summary>
        /// Loads all data base entries of objects with type 'T'. The result object enables fluent api calls for filtering.
        /// </summary>
        /// <returns>A new instance of the <see cref="SelectApi{T}"/> class.</returns>
        public SelectApi<T> LoadAll()
        {
            IEnumerable<T> allEntries = _selectManager.GetAll(typeof(T)).ToList();
            return new SelectApi<T>(allEntries);
        }

        /// <summary>
        /// Loads all data base entries of objects with type 'T' filtered by a single property. The result object enables fluent api calls for filtering.
        /// </summary>
        /// <returns>A new instance of the <see cref="SelectApi{T}"/> class.</returns>
        public SelectApi<T> LoadAllWhere(string propertyName, CompareOption compareOption, double compareValue)
        {
            IEnumerable<T> entries = _selectManager.GetAllWhere(typeof(T), propertyName, compareOption, compareValue).ToList();
            return new SelectApi<T>(entries);
        }

        /// <summary>
        /// Loads a data base entry belonging to a primary key.
        /// </summary>
        /// <param name="primaryKeyValue"></param>
        /// <returns></returns>
        public T LoadByPrimaryKey(int primaryKeyValue)
        {
            return (T)_selectManager.GetByPrimaryKey(typeof(T), primaryKeyValue);
        }
    }
}
