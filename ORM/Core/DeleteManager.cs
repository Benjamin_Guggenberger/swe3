﻿using System.Collections.Generic;
using ORM.Cache.Interface;
using ORM.DataAccess.Interface;
using ORM.Helper;
using ORM.MetaData;
using ORM.MetaData.Interface;

namespace ORM.Core
{
    public class DeleteManager<T> : CrudManager
        where T : class, new()
    {
        private readonly ICacheRegister _cacheRegister;

        public DeleteManager(IMetaDataRegister metaDataRegister, 
            ICacheRegister cacheRegister, 
            IQueryExecutor queryExecutor) 
            : base(metaDataRegister, queryExecutor)
        {
            _cacheRegister = cacheRegister;
        }

        public void Delete(T obj)
        {
            var queryBuilder = new SqlDeleteQueryBuilder();

            MetaDataInfoTable tableInfo = GetMetaDataInfo(typeof(T));

            int primaryKeyValue = GetPrimaryKeyValue(obj, tableInfo);

            queryBuilder.AppendDeleteFrom(tableInfo.TableName, tableInfo.PrimaryKey);
            string deleteQuery = queryBuilder.GetSqlQuery();

            IDictionary<string, object> primaryKeyValueByColumnName = new Dictionary<string, object> {[tableInfo.PrimaryKey] = primaryKeyValue};
            var command = QueryExecutor.PrepareParameters(deleteQuery, primaryKeyValueByColumnName);
            QueryExecutor.ExecuteNonQuery(command);

            _cacheRegister.Remove(typeof(T), primaryKeyValue, tableInfo.TableName);
        }
    }
}
