﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ORM.Cache.Interface;
using ORM.DataAccess.Interface;
using ORM.Helper;
using ORM.MetaData;
using ORM.MetaData.Interface;

namespace ORM.Core
{
    /// <summary>
    /// Represents a class that manages data base upserts (insert or update).
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UpsertManager<T> : CrudManager
     where T : class, new()
    {
        private readonly ICacheRegister _cacheRegister;

        public UpsertManager(IMetaDataRegister metaDataRegister,
            ICacheRegister cacheRegister,
            IQueryExecutor queryExecutor) 
            : base(metaDataRegister, queryExecutor)
        {
            _cacheRegister = cacheRegister;
        }

        /// <summary>
        /// Inserts or updates an object into the data base.
        /// </summary>
        /// <param name="obj">The object that gets inserted or updated.</param>
        /// <returns>The inserted object.</returns>
        public object Upsert(object obj)
        {
            Type type = obj.GetType();
            MetaDataInfoTable tableInfo = GetMetaDataInfo(type);

            UpsertHelper helper = new UpsertHelper
            {
                TableName = tableInfo.TableName,
                PrimaryKey = tableInfo.PrimaryKey
            };

            IterateProperties(obj, tableInfo, helper);

            string upsertQuery = new SqlUpsertQueryBuilder(helper).GetSqlQuery();

            IDbCommand command = QueryExecutor.PrepareParameters(upsertQuery, helper.ValueByColumnNameDictionary);
            QueryExecutor.ExecuteNonQuery(command);

            int primaryKeyValue = GetPrimaryKeyValue(obj, tableInfo);
            HandleNToMRelations(obj, tableInfo, primaryKeyValue);

            _cacheRegister.AddOrUpdate(type, primaryKeyValue, obj);

            return obj;
        }

        private void IterateProperties(object obj, MetaDataInfoTable tableInfo, UpsertHelper helper)
        {
            foreach (MetaDataInfoColumn columnInfo in tableInfo.Columns)
            {
                object value = columnInfo.PropertyInfo.GetValue(obj);

                // upsert foreign key column
                if (columnInfo.IsForeignKey)
                {
                    object relatedObject = value;
                    if (relatedObject == null)
                    {
                        helper.ValueByColumnNameDictionary.TryAdd(columnInfo.ColumnName, relatedObject);
                        continue;
                    }

                    relatedObject = Upsert(relatedObject);

                    int foreignKeyValue = GetPrimaryKeyValueOfRelatedTable(relatedObject);

                    helper.ValueByColumnNameDictionary.TryAdd(columnInfo.ColumnName, foreignKeyValue);
                    continue;
                }

                // determines if the column is in fact a database column
                if (!columnInfo.IsList1ToN && !columnInfo.IsListNToM)
                {
                    helper.ValueByColumnNameDictionary.TryAdd(columnInfo.ColumnName, value);
                }
            }
        }

        private void HandleNToMRelations(object obj, MetaDataInfoTable tableInfo, object primaryKeyValue1)
        {
            // updating n:m relations works only if the entity to update itself has this relation
            if (obj.GetType() != typeof(T))
            {
                return;
            }

            IEnumerable<MetaDataInfoColumn> columnInfos = tableInfo.Columns.Where(info => info.IsListNToM);
            foreach (MetaDataInfoColumn columnInfo in columnInfos)
            {
                IList relatedObjects = (IList) columnInfo.PropertyInfo.GetValue(obj);
                if (relatedObjects != null)
                {
                    foreach (object relatedObject in relatedObjects)
                    {
                        UpsertNToM(obj, relatedObject, tableInfo, columnInfo, primaryKeyValue1);
                    }
                }
            }
        }

        private void UpsertNToM(object obj, object relatedObject, MetaDataInfoTable tableInfo, MetaDataInfoColumn columnInfo, object primaryKeyValue1)
        {
            Type relatedType = relatedObject.GetType();
            MetaDataInfoTable relatedTableInfo = GetMetaDataInfo(relatedType);

            MetaDataInfoColumn relatedColumnInfo = 
                relatedTableInfo.Columns.SingleOrDefault(c => c.IsListNToM && c.RelatedTableName == tableInfo.TableName);
            if (relatedColumnInfo != null)
            {
                IList objects = (IList)relatedColumnInfo.PropertyInfo.GetValue(relatedObject);
                if (objects != null)
                {
                    if (!objects.Contains(obj))
                    {
                        var newList = new List<T> { (T)obj };
                        newList.AddRange((IList<T>)objects);
                        relatedColumnInfo.PropertyInfo.SetValue(relatedObject, newList);
                    }
                }
                else
                {
                    relatedColumnInfo.PropertyInfo.SetValue(relatedObject, new List<T> { (T)obj });
                }

                Upsert(relatedObject);
                object primaryKeyValue2 = GetPrimaryKeyValue(relatedObject, relatedTableInfo);

                string upsertQuery = new SqlUpsertQueryBuilder()
                    .GetSqlQueryForJoinTable(columnInfo.JoinTableName,
                        new KeyValuePair<string, object>(tableInfo.PrimaryKey, primaryKeyValue1),
                        new KeyValuePair<string, object>(relatedTableInfo.PrimaryKey, primaryKeyValue2));
                QueryExecutor.ExecuteNonQuery(upsertQuery);
            }
        }

        private int GetPrimaryKeyValueOfRelatedTable(object obj)
        {
            Type type = obj.GetType();
            MetaDataInfoTable tableInfo = GetMetaDataInfo(type);

            return GetPrimaryKeyValue(obj, tableInfo);
        }
    }
}
