﻿using System.Data;
using ORM.Cache;
using ORM.Cache.Interface;
using ORM.DataAccess;
using ORM.DataAccess.Interface;
using ORM.MetaData;
using ORM.MetaData.Interface;
using ORM.Transaction;
using ORM.Transaction.Interface;

namespace ORM.Core
{
    /// <summary>
    /// Represents the parent class with the context of the entities.
    /// </summary>
    public class Context
    {
        private readonly IMetaDataRegister _metaDataRegister;
        private readonly ICacheRegister _cacheRegister;
        private readonly IQueryExecutor _queryExecutor;

        public ITransactionProvider SqlTransactionProvider { get; }

        protected Context(IDbConnection connection)
        {
            SqlTransactionProvider = new SqlTransactionProvider(connection);
            _queryExecutor = new SqlQueryExecutor(connection, SqlTransactionProvider);
            _metaDataRegister = new MetaDataRegister();
            _cacheRegister = new CacheRegister(_metaDataRegister);
        }

        protected Entity<T> Initialize<T>() 
            where T : class, new()
        {
            return new Entity<T>(_metaDataRegister, _cacheRegister, _queryExecutor);
        }
    }
}
