﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ORM.DataAccess.Interface;
using ORM.Helper;
using ORM.MetaData;
using ORM.MetaData.Interface;

namespace ORM.Core
{
    /// <summary>
    /// Represents a class that manages the creation of data base tables.
    /// </summary>
    public class CreateManager<T> : CrudManager
        where T : class, new()
    {
        public CreateManager(IMetaDataRegister metaDataRegister, IQueryExecutor queryExecutor)
            : base(metaDataRegister, queryExecutor)
        {
        }

        /// <summary>
        /// Creates a new data base table if it didn't exist already.
        /// </summary>
        public void CreateTable()
        {
            Type type = GetType().GenericTypeArguments.Single();

            PropertyInfo[] properties = type.GetProperties();

            var queryBuilder = new SqlCreateTableQueryBuilder();

            var tableInfo = new MetaDataInfoTable {TableName = type.Name};

            queryBuilder.AppendTableExistenceCheck(type.Name);
            IterateProperties(properties, tableInfo, queryBuilder);

            MetaDataValidator.Validate(tableInfo);
            MetaDataRegister.Register(type, tableInfo);
            queryBuilder.AppendPrimaryKeys(new List<string>{tableInfo.PrimaryKey});

            string createTableQuery = queryBuilder.GetSqlQuery();
            QueryExecutor.ExecuteNonQuery(createTableQuery);

            HandleNToMRelations(tableInfo, type);
        }

        private void IterateProperties(PropertyInfo[] properties,
            MetaDataInfoTable tableInfo, SqlCreateTableQueryBuilder queryBuilder)
        {
            foreach (PropertyInfo property in properties)
            {
                var propertyInfo = new MetaDataInfoColumn();

                propertyInfo.SetMetaDataInfo(property, tableInfo);
                tableInfo.Columns.Add(propertyInfo);

                if (!propertyInfo.IsList1ToN && !propertyInfo.IsListNToM)
                {
                    queryBuilder.AppendCreateTable(propertyInfo);
                }
            }
        }

        private void HandleNToMRelations(MetaDataInfoTable tableInfo, Type type)
        {
            foreach (MetaDataInfoColumn columnInfo in tableInfo.Columns)
            {
                if (columnInfo.JoinTableName != null)
                {
                    if (RelatedTableExists(columnInfo.RelatedTableType, out MetaDataInfoTable relatedTableInfo))
                    {
                        MetaDataInfoColumn relatedColumnInfo = relatedTableInfo.Columns.FirstOrDefault(c => c.RelatedTableType == type && c.IsListNToM);
                        if (relatedColumnInfo != null)
                        {
                            if (relatedColumnInfo.JoinTableName != columnInfo.JoinTableName)
                            {
                                throw new InvalidOperationException("Join Table names do not match.");
                            }

                            string createJoinTableQuery = CreateJoinTable(columnInfo.JoinTableName, tableInfo.TableName, columnInfo.RelatedTableName);
                            QueryExecutor.ExecuteNonQuery(createJoinTableQuery);
                        }
                        else
                        {
                            throw new InvalidOperationException("Invalid usage of 'ListNToM' Attribute.");
                        }
                    }
                }
            }
        }

        private bool RelatedTableExists(Type t, out MetaDataInfoTable relatedTableInfo)
        {
            return MetaDataRegister.TryGetValue(t, out relatedTableInfo);
        }

        private string CreateJoinTable(string joinTableName, string relatedTable1, string relatedTable2)
        {
            var queryBuilder = new SqlCreateTableQueryBuilder();
            var foreignKey1 = $"Fk_{relatedTable1}Id";
            var foreignKey2 = $"Fk_{relatedTable2}Id";

            queryBuilder.AppendTableExistenceCheck(joinTableName);
            queryBuilder.AppendCreateJoinTable(foreignKey1, relatedTable1);
            queryBuilder.AppendCreateJoinTable(foreignKey2, relatedTable2);
            queryBuilder.AppendPrimaryKeys(new List<string> { foreignKey1, foreignKey2 });

            return queryBuilder.GetSqlQuery();
        }
    }
}
