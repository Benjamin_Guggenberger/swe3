﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ORM.Cache.Interface;
using ORM.DataAccess.Interface;
using ORM.Helper;
using ORM.MetaData;
using ORM.MetaData.Interface;

namespace ORM.Core
{
    /// <summary>
    /// Represents a class that manages data base queries.
    /// </summary>
    /// <typeparam name="T">The c# class that belongs to the data base table which should be queried.</typeparam>
    public class SelectManager<T> : CrudManager
        where T : class, new()
    {
        private readonly ICacheRegister _cacheRegister;

        public SelectManager(IMetaDataRegister metaDataRegister,
            ICacheRegister cacheRegister,
            IQueryExecutor queryExecutor)
            : base(metaDataRegister, queryExecutor)
        {
            _cacheRegister = cacheRegister;
        }

        /// <summary>
        /// Gets all data base entries belonging to a c# class.
        /// </summary>
        /// <param name="type">The type of the c# class.</param>
        /// <returns>A container with all existing entries.</returns>
        public IEnumerable<T> GetAll(Type type)
        {
            MetaDataInfoTable tableInfo = GetMetaDataInfo(type);

            SqlSelectQueryBuilder queryBuilder = new SqlSelectQueryBuilder();
            queryBuilder.AppendSelectAll(type.Name);

            string sqlSelectQuery = queryBuilder.GetSqlQuery();
            IDataReader reader = QueryExecutor.ExecuteReader(sqlSelectQuery);

            IEnumerable<object> entries = ReadEntries(tableInfo, type, reader);
            if (entries != null)
            {
                foreach (object entry in entries)
                {
                    yield return (T)entry;
                }
            }
        }

        /// <summary>
        /// Gets the data base entries belonging to a c# class filtered by a single property.
        /// </summary>
        /// <param name="type">The type of the # class.</param>
        /// <param name="propertyName">The name of the property where the comparison is applied.</param>
        /// <param name="compareOption">The kind of comparison.</param>
        /// <param name="compareValue">The compare value.</param>
        /// <returns>A container with the existing entries.</returns>
        public IEnumerable<T> GetAllWhere(Type type, string propertyName, CompareOption compareOption, double compareValue)
        {
            MetaDataInfoTable tableInfo = GetMetaDataInfo(type);

            SqlSelectQueryBuilder queryBuilder = new SqlSelectQueryBuilder();

            string filterOptionString = Converter.GetCompareOptionString(compareOption);

            MetaDataInfoColumn columnInfo = tableInfo.Columns.FirstOrDefault(
                c => c.ColumnName == propertyName ||
                     c.IsForeignKey && c.ColumnName == $"Fk_{propertyName}Id");
            if (columnInfo == null)
            {
                throw new InvalidOperationException($"Column name '{propertyName}' not found.");
            }

            if (columnInfo.DataType != Constants.DataTypes.Integer && columnInfo.DataType != Constants.DataTypes.Double)
            {
                throw new InvalidOperationException("Filter only allowed for properties with numeric data types.");
            }

            if (columnInfo.IsForeignKey)
            {
                propertyName = $"Fk_{propertyName}Id";
            }
            queryBuilder.AppendSelectWhere(type.Name, propertyName, filterOptionString, compareValue);

            string sqlSelectQuery = queryBuilder.GetSqlQuery();
            IDataReader reader = QueryExecutor.ExecuteReader(sqlSelectQuery);

            IEnumerable<object> entries = ReadEntries(tableInfo, type, reader);
            if (entries != null)
            {
                foreach (object entry in entries)
                {
                    yield return (T)entry;
                }
            }
        }

        /// <summary>
        /// Gets the data base entry with a given primary key belonging to a c# class.
        /// </summary>
        /// <param name="type">The type of the c# class.</param>
        /// <param name="primaryKeyValue">The primary key.</param>
        /// <returns>The entry.</returns>
        public object GetByPrimaryKey(Type type, int primaryKeyValue)
        {
            // get object from cache if possible
            if (_cacheRegister.TryGetValue(type, primaryKeyValue, out object obj))
            {
                return obj;
            }

            MetaDataInfoTable tableInfo = GetMetaDataInfo(type);

            SqlSelectQueryBuilder queryBuilder = new SqlSelectQueryBuilder();
            queryBuilder.AppendSelectByPrimaryKey(tableInfo.TableName, tableInfo.PrimaryKey, primaryKeyValue);

            string sqlSelectQuery = queryBuilder.GetSqlQuery();
            IDataReader reader = QueryExecutor.ExecuteReader(sqlSelectQuery);
            IEnumerable<object> entries = ReadEntries(tableInfo, type, reader);
            return entries.FirstOrDefault();
        }

        private IEnumerable<object> ReadEntries(MetaDataInfoTable tableInfo, Type type, IDataReader reader)
        {
            int primaryKeyValue = 0;
            while (reader.Read())
            {
                IDictionary<string, object> valuesByColumnNameDictionary = new Dictionary<string, object>();

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    valuesByColumnNameDictionary.TryAdd(reader.GetName(i), reader.GetValue(i));
                }

                object obj = Activator.CreateInstance(type);
                foreach (MetaDataInfoColumn columnInfo in tableInfo.Columns)
                {
                    if (valuesByColumnNameDictionary.TryGetValue(columnInfo.ColumnName, out object value))
                    {
                        if (columnInfo.IsPrimaryKey)
                        {
                            primaryKeyValue = (int)value;
                        }
                        if (columnInfo.IsForeignKey)
                        {
                            value = value == DBNull.Value ? null : GetByPrimaryKey(columnInfo.RelatedTableType, (int)value);
                        }
                        columnInfo.PropertyInfo.SetValue(obj, value);
                    }
                }

                HandleNToMRelations(obj, primaryKeyValue, tableInfo);

                yield return obj;
            }
        }

        private void HandleNToMRelations(object obj, int primaryKeyValue, MetaDataInfoTable tableInfo)
        {
            IEnumerable<MetaDataInfoColumn> columnInfos = tableInfo.Columns.Where(c => c.IsListNToM);
            foreach (MetaDataInfoColumn columnInfo in columnInfos)
            {
                SqlSelectQueryBuilder queryBuilder = new SqlSelectQueryBuilder();
                queryBuilder.AppendSelectJoinTable(columnInfo.JoinTableName, tableInfo.TableName, primaryKeyValue);

                string sqlSelectQuery = queryBuilder.GetSqlQuery();
                IDataReader reader = QueryExecutor.ExecuteReader(sqlSelectQuery);

                var values = ReadJoinTableEntries(columnInfo, reader).ToList();

                Type listType = typeof(List<>).MakeGenericType(columnInfo.RelatedTableType);
                var relatedObjects = (IList)Activator.CreateInstance(listType);

                foreach (object value in values)
                {
                    relatedObjects.Add(value);
                }


                columnInfo.PropertyInfo.SetValue(obj, relatedObjects);
            }
        }

        private IEnumerable<object> ReadJoinTableEntries(MetaDataInfoColumn columnInfo, IDataReader reader)
        {
            while (reader.Read())
            {
                IDictionary<string, int> valuesByColumnNameDictionary = new Dictionary<string, int>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    valuesByColumnNameDictionary.TryAdd(reader.GetName(i), (int)reader.GetValue(i));
                }

                Type type = columnInfo.RelatedTableType;
                MetaDataInfoTable tableInfo = GetMetaDataInfo(type);

                var columnName = $"Fk_{tableInfo.TableName}Id";
                int relatedTablePrimaryKey = valuesByColumnNameDictionary.Where(kvp => kvp.Key == columnName)
                    .Select(kvp => kvp.Value).Single();

                object obj = GetByPrimaryKey(type, relatedTablePrimaryKey);
                yield return obj;
            }
        }
    }
}
