﻿using System;
using System.Linq;
using ORM.DataAccess.Interface;
using ORM.MetaData;
using ORM.MetaData.Interface;

namespace ORM.Core
{
    /// <summary>
    /// Represents the base class for managing data base interactions.
    /// </summary>
    public abstract class CrudManager
    {
        protected readonly IQueryExecutor QueryExecutor;
        protected readonly IMetaDataRegister MetaDataRegister;

        protected CrudManager(IMetaDataRegister metaDataRegister, IQueryExecutor queryExecutor)
        {
            QueryExecutor = queryExecutor;
            MetaDataRegister = metaDataRegister;
        }

        protected MetaDataInfoTable GetMetaDataInfo(Type type)
        {
            if (!MetaDataRegister.TryGetValue(type, out var tableInfo))
            {
                throw new InvalidOperationException("Table not registered.");
            }

            return tableInfo;
        }

        protected int GetPrimaryKeyValue(object obj, MetaDataInfoTable tableInfo)
        {
            var primaryKeyProperty = tableInfo.Columns.Single(t => t.IsPrimaryKey).PropertyInfo;
            return (int)primaryKeyProperty.GetValue(obj);
        }
    }
}
