﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ORM.Core
{
    public class SelectApi<T> where T : class, new()
    {
        private IEnumerable<T> _allEntries;
        public SelectApi(IEnumerable<T> allEntries)
        {
            _allEntries = allEntries;
        }

        /// <summary>
        /// Filters objects.
        /// </summary>
        /// <param name="filterFunc">The filter function.</param>
        /// <returns>The entity class itself.</returns>
        public SelectApi<T> Where(Func<T, bool> filterFunc)
        {
            _allEntries = _allEntries.Where(filterFunc);
            return this;
        }

        /// <summary>
        /// Orders the objects.
        /// </summary>
        /// <typeparam name="TProperty">The property by which should be order.</typeparam>
        /// <param name="orderFunc">The order function.</param>
        /// <param name="isAscending">Order direction.</param>
        /// <returns>The entity class itself.</returns>
        public SelectApi<T> OrderBy<TProperty>(Func<T, TProperty> orderFunc, bool isAscending = true)
        {
            _allEntries = isAscending ? _allEntries.OrderBy(orderFunc) : _allEntries.OrderByDescending(orderFunc);
            return this;
        }

        /// <summary>
        /// Gets the objects.
        /// </summary>
        /// <returns>The object.</returns>
        public IEnumerable<T> GetObjects()
        {
            return _allEntries;
        }

        /// <summary>
        /// Gets the property of an object.
        /// </summary>
        /// <typeparam name="TProperty">The property.</typeparam>
        /// <param name="selectionFunc">The selection function.</param>
        /// <returns>The property.</returns>
        public IEnumerable<TProperty> GetProperty<TProperty>(Func<T, TProperty> selectionFunc)
        {
            return _allEntries.Select(selectionFunc);
        }
    }
}