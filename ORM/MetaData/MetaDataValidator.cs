﻿using System;
using ORM.Helper;

namespace ORM.MetaData
{
    public static class MetaDataValidator
    {
        public static void Validate(MetaDataInfoTable tableInfo)
        {
            int primaryKeysCount = 0;

            foreach (MetaDataInfoColumn columnInfo in tableInfo.Columns)
            {
                if (columnInfo.PropertyInfo.PropertyType != typeof(string) && columnInfo.DataType != null && columnInfo.DataType.Contains("VARCHAR", StringComparison.OrdinalIgnoreCase))
                {
                    throw new InvalidOperationException("'MaxLength' Attribute only allowed with type \"string\"");
                }
                if (columnInfo.IsPrimaryKey)
                {
                    if (columnInfo.PropertyInfo.PropertyType != typeof(int))
                    {
                        throw new InvalidOperationException("'Primary Key' must be of type \"int\"");
                    }
                    if (!columnInfo.PropertyInfo.Name.EndsWith("Id", StringComparison.OrdinalIgnoreCase))
                    {
                        throw new InvalidOperationException("'PrimaryKey' Attribute has to end with \"Id\"");
                    }
                    primaryKeysCount++;
                    if (primaryKeysCount > 1)
                    {
                        throw new InvalidOperationException("Table cannot contain more than one primary key.");
                    }

                    if (columnInfo.IsListNToM || columnInfo.IsList1ToN)
                    {
                        throw new InvalidOperationException("A PrimaryKey can not have a 'List1ToN' or 'ListNToM' Attribute.");
                    }
                }
                if (columnInfo.IsForeignKey)
                {
                    if (columnInfo.IsListNToM || columnInfo.IsList1ToN)
                    {
                        throw new InvalidOperationException("A ForeignKey can not have a 'List1ToN' or 'ListNToM' Attribute.");
                    }

                    if (columnInfo.RelatedTableType.IsPrimitive || columnInfo.RelatedTableType == typeof(string) ||
                        columnInfo.RelatedTableType == typeof(decimal) || columnInfo.DataType == null)
                    {
                        throw new InvalidOperationException("'ForeignKey' Attribute used at invalid property.");
                    }
                }
                if (columnInfo.IsList1ToN)
                {
                    if (columnInfo.IsListNToM)
                    {
                        throw new InvalidOperationException("Property cannot have both 'List1ToN' and 'ListNToM' Attributes.");
                    }
                    if (columnInfo.DataType != null)
                    {
                        throw new InvalidOperationException("'List1ToN' Attribute used at invalid property.");
                    }
                }
                if (columnInfo.IsListNToM && columnInfo.DataType != null)
                {
                    throw new InvalidOperationException("'ListNToM' Attribute used at invalid property.");
                }
                if (columnInfo.NotNull && columnInfo.DeleteOption == DeleteOption.SetNull)
                {
                    throw new InvalidOperationException("ForeignKey cannot have 'NotNull' and 'ON DELETE SET NULL' Attribute.");
                }
            }

            if (primaryKeysCount == 0)
            {
                throw new InvalidOperationException("Table must contain a PrimaryKey.");
            }
        }
    }
}
