﻿using System;

namespace ORM.MetaData.Interface
{
    public interface IMetaDataRegister
    {
        public void Register(Type type, MetaDataInfoTable info);
        public bool TryGetValue(Type type, out MetaDataInfoTable info);
        public bool Contains(Type type);
    }
}
