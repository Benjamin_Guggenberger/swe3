﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using ORM.MetaData.Interface;

namespace ORM.MetaData
{
    /// <summary>
    /// Represents a class for the meta data register.
    /// </summary>
    public class MetaDataRegister : IMetaDataRegister
    {
        private readonly IDictionary<Type, MetaDataInfoTable> _metaDataByType;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetaDataRegister"/> class.
        /// </summary>
        public MetaDataRegister()
        {
            _metaDataByType = new ConcurrentDictionary<Type, MetaDataInfoTable>();
        }

        /// <summary>
        /// Adds a meta data info to the dictionary.
        /// </summary>
        /// <param name="type">The key of the dictionary.</param>
        /// <param name="info">The value of the dictionary.</param>
        public void Register(Type type, MetaDataInfoTable info)
        {
            _metaDataByType.TryAdd(type, info);
        }

        /// <summary>
        /// Tries to the get meta data info of a given type.
        /// </summary>
        /// <param name="type">The given type.</param>
        /// <param name="info">The meta data info.</param>
        /// <returns>True if meta data info exists.</returns>
        public bool TryGetValue(Type type, out MetaDataInfoTable info)
        {
            return _metaDataByType.TryGetValue(type, out info);
        }

        /// <summary>
        /// Checks if meta data info exist to a given type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>True if meta data info exists.</returns>
        public bool Contains(Type type)
        {
            return _metaDataByType.ContainsKey(type);
        }
    }
}
