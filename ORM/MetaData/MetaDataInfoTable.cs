﻿using System;
using System.Collections.Generic;

namespace ORM.MetaData
{
    /// <summary>
    /// Represents a class that holds meta data info about a class.
    /// </summary>
    public class MetaDataInfoTable
    {
        /// <summary>
        /// The name of the table.
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// The primary keys of the table.
        /// </summary>
        public string PrimaryKey { get; set; }

        /// <summary>
        /// The properties of the class.
        /// </summary>
        public ICollection<MetaDataInfoColumn> Columns { get; }

        public MetaDataInfoTable()
        {
            Columns = new List<MetaDataInfoColumn>();
        }
    }
}