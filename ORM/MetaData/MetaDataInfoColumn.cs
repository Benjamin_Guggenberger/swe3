﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ORM.Helper;

namespace ORM.MetaData
{
    /// <summary>
    /// Represents a class that holds meta data information about a class property.
    /// </summary>
    public class MetaDataInfoColumn
    {
        /// <summary>
        /// The name of the column.
        /// </summary>
        public string ColumnName { get; private set; }

        /// <summary>
        /// The property info of the property.
        /// </summary>
        public PropertyInfo PropertyInfo { get; private set; }

        /// <summary>
        /// The data base data type of the property.
        /// </summary>
        public string DataType { get; private set; }

        /// <summary>
        /// Marks if the column is nullable.
        /// </summary>
        public bool NotNull { get; private set; }

        /// <summary>
        /// Marks if the column is a primary key.
        /// </summary>
        public bool IsPrimaryKey { get; private set; }

        /// <summary>
        /// Marks if the column is a foreign key.
        /// </summary>
        public bool IsForeignKey { get; private set; }

        /// <summary>
        /// The delete option of the foreign key.
        /// </summary>
        public DeleteOption DeleteOption { get; private set; }

        /// <summary>
        /// Marks if the column has a one to many relation.
        /// </summary>
        public bool IsList1ToN { get; private set; }

        /// <summary>
        /// Marks if the column has a many to many relation.
        /// </summary>
        public bool IsListNToM { get; private set; }

        /// <summary>
        /// The join table name to the column.
        /// </summary>
        public string JoinTableName { get; private set; }

        /// <summary>
        /// The related table name of the column.
        /// </summary>
        public string RelatedTableName { get; private set; }

        /// <summary>
        /// The related table type of the column.
        /// </summary>
        public Type RelatedTableType { get; private set; }


        /// <summary>
        /// Sets the attributes for the property.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="metaDataInfoTable"></param>
        public void SetMetaDataInfo(PropertyInfo property, MetaDataInfoTable metaDataInfoTable)
        {
            Type propertyType = property.PropertyType;

            PropertyInfo = property;
            ColumnName = property.Name;
            SetDataType(property);

            IEnumerable<Tuple<string, string>> attributes = GetAttributes(property);

            foreach (Tuple<string, string> attribute in attributes)
            {
                if (attribute.Item1 == Constants.Attributes.PrimaryKey)
                {
                    IsPrimaryKey = true;
                    metaDataInfoTable.PrimaryKey = property.Name;
                }
                else if (attribute.Item1 == Constants.Attributes.ForeignKey)
                {
                    ColumnName = $"Fk_{propertyType.Name}Id";
                    RelatedTableName = propertyType.Name;
                    RelatedTableType = propertyType;
                    IsForeignKey = true;
                    DeleteOption = Enum.Parse<DeleteOption>(attribute.Item2);

                }
                else if (attribute.Item1 == Constants.Attributes.List1ToN)
                {
                    IsList1ToN = true;

                    Type type = propertyType.GenericTypeArguments.FirstOrDefault();
                    if (type != null)
                    {
                        RelatedTableName = type.Name;
                    }
                }
                else if (attribute.Item1 == Constants.Attributes.ListNToM)
                {
                    IsListNToM = true;

                    Type relatedTableType = propertyType.GenericTypeArguments.FirstOrDefault();
                    if (relatedTableType != null)
                    {
                        JoinTableName = attribute.Item2;
                        RelatedTableName = relatedTableType.Name;
                        RelatedTableType = relatedTableType;
                    }
                }
                else if (attribute.Item1 == Constants.Attributes.StringMaxLength)
                {
                    DataType = string.Concat("VARCHAR(", Convert.ToInt32(attribute.Item2), ")");
                }
                else if (attribute.Item1 == Constants.Attributes.NotNull)
                {
                    NotNull = true;
                }
            }
        }

        private void SetDataType(PropertyInfo property)
        {
            // get data type for data base
            DataType = Converter.GetDbDataType(property.PropertyType);
        }

        private IEnumerable<Tuple<string, string>> GetAttributes(MemberInfo property)
        {
            IList<CustomAttributeData> attributes = property.GetCustomAttributesData();
            foreach (var attribute in attributes)
            {
                string attributeName = attribute.AttributeType.Name;
                string constructorArgument = null;

                if (AttributeHasConstructorArgument(attributeName))
                {
                    CustomAttributeTypedArgument typedArgument = attribute.ConstructorArguments.FirstOrDefault();
                    constructorArgument = typedArgument.Value?.ToString();
                }
                yield return new Tuple<string, string>(attributeName, constructorArgument);
            }
        }

        private bool AttributeHasConstructorArgument(string attrName)
        {
            return attrName == Constants.Attributes.ListNToM ||
                   attrName == Constants.Attributes.ForeignKey ||
                   attrName == Constants.Attributes.StringMaxLength;
        }
    }
}
