﻿using System;

namespace ORM.Cache.Interface
{
    public interface ICacheRegister
    {
        public void AddType(Type type);
        public void AddOrUpdate(Type type, int primaryKey, object obj);
        public bool TryGetValue(Type type, int primaryKeyValue, out object obj);
        public void Remove(Type type, int primaryKeyValue, string tableName);
    }
}
