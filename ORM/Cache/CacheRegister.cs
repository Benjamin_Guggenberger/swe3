﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using ORM.Cache.Interface;
using ORM.Helper;
using ORM.MetaData;
using ORM.MetaData.Interface;

namespace ORM.Cache
{
    public class CacheRegister : ICacheRegister
    {
        private readonly IMetaDataRegister _metaDataRegister;
        private readonly IDictionary<Type, IDictionary<int, object>> _objectByPrimaryKeyByTypeDictionary;
        private readonly IDictionary<Type, IDictionary<int, string>> _hashCodeByPrimaryKeyByTypeDictionary;

        public CacheRegister(IMetaDataRegister metaDataRegister)
        {
            _metaDataRegister = metaDataRegister;
            _objectByPrimaryKeyByTypeDictionary = new ConcurrentDictionary<Type, IDictionary<int, object>>();
            _hashCodeByPrimaryKeyByTypeDictionary = new ConcurrentDictionary<Type, IDictionary<int, string>>();
        }

        /// <summary>
        /// Adds or updates an object to the cache.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="primaryKey">The primary key.</param>
        /// <param name="obj">The object.</param>
        public void AddOrUpdate(Type type, int primaryKey, object obj)
        {
            IDictionary<int, object> objectByPrimaryKey = _objectByPrimaryKeyByTypeDictionary[type];
            IDictionary<int, string> hashCodeByPrimaryKey = _hashCodeByPrimaryKeyByTypeDictionary[type];
         
            string newHashCode = GetHashCode(obj);

            if (objectByPrimaryKey.TryAdd(primaryKey, obj))
            {
                hashCodeByPrimaryKey.Add(primaryKey, newHashCode);
            }
            else
            {
                if (HasObjectHashCodeChanged(primaryKey, newHashCode, hashCodeByPrimaryKey))
                {
                    UpdateHashCodeByPrimaryKey(primaryKey, newHashCode, hashCodeByPrimaryKey);
                    UpdateObjectByPrimaryKey(primaryKey, obj, objectByPrimaryKey);
                }
            }
        }

        /// <summary>
        /// Adds a type to the register.
        /// </summary>
        /// <param name="type">The type - representing the entities from the database.</param>
        public void AddType(Type type)
        {
            _objectByPrimaryKeyByTypeDictionary.Add(type, new ConcurrentDictionary<int, object>());
            _hashCodeByPrimaryKeyByTypeDictionary.Add(type, new ConcurrentDictionary<int, string>());
        }

        /// <summary>
        /// Tries to load the object to a given primary key from the cache.
        /// </summary>
        /// <param name="type">The type of the object.</param>
        /// <param name="primaryKeyValue">The primary key of the object.</param>
        /// <param name="obj">The object.</param>
        /// <returns>True if the object is in the cache, otherwise false.</returns>
        public bool TryGetValue(Type type, int primaryKeyValue, out object obj)
        {
            if (_objectByPrimaryKeyByTypeDictionary.TryGetValue(type, out IDictionary<int, object> objectByPrimaryKey))
            {
                if (objectByPrimaryKey.TryGetValue(primaryKeyValue, out obj))
                {
                    return true;
                }
            }

            obj = null;
            return false;
        }

        /// <summary>
        /// Removes an object from the cache when it is removed from the data base.
        /// </summary>
        /// <param name="type">The type of the object.</param>
        /// <param name="primaryKeyValue">The primary key of the object.</param>
        /// <param name="tableName"></param>
        public void Remove(Type type, int primaryKeyValue, string tableName)
        {
            IDictionary<int, object> objectByPrimaryKey = _objectByPrimaryKeyByTypeDictionary[type];
            if (objectByPrimaryKey.ContainsKey(primaryKeyValue))
            {
                objectByPrimaryKey.Remove(primaryKeyValue);
            }

            ResetCacheForRelatedTables(tableName);
        }

        /// <summary>
        /// Updates the cache info of the object to a given primary key.
        /// </summary>
        /// <param name="primaryKey">The primary key.</param>
        /// <param name="obj">The new cache info.</param>
        /// <param name="objectByPrimaryKey">The cache info by primary key dictionary.</param>
        private void UpdateObjectByPrimaryKey(int primaryKey, object obj, IDictionary<int, object> objectByPrimaryKey)
        {
            objectByPrimaryKey.Remove(primaryKey);
            objectByPrimaryKey.Add(primaryKey, obj);
        }

        /// <summary>
        /// Updates the hash code of the object to a given primary key.
        /// </summary>
        /// <param name="primaryKey">The primary key.</param>
        /// <param name="hashCode">The new hash code.</param>
        /// <param name="hashCodeByPrimaryKey">The hash code by primary key dictionary.</param>
        private void UpdateHashCodeByPrimaryKey(int primaryKey, string hashCode, IDictionary<int, string> hashCodeByPrimaryKey)
        {
            hashCodeByPrimaryKey.Remove(primaryKey);
            hashCodeByPrimaryKey.Add(primaryKey, hashCode);
        }

        /// <summary>
        /// Computes the hash code of the object to a given primary key.
        /// </summary>
        /// <param name="obj">the object.</param>
        /// <returns></returns>
        private string GetHashCode(object obj)
        {
            Type type = obj.GetType();
            string toStringProperties = String.Empty;
            foreach (PropertyInfo property in type.GetProperties())
            {
                object value = property.GetValue(obj);
                toStringProperties += value?.ToString();
            }

            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(toStringProperties));
                string hashCode = null;

                foreach (byte b in bytes)
                {
                    hashCode += b.ToString("x2");
                }
                return hashCode;
            }
        }

        /// <summary>
        /// Checks if the hash code of the object to a given primary key has changed.
        /// </summary>
        /// <param name="hashCodeByPrimaryKey"></param>
        /// <param name="primaryKey">The primary key.</param>
        /// <param name="newHashCode">The new hash code.</param>
        /// <returns>True if the hash code of the object has changed.</returns>
        private bool HasObjectHashCodeChanged(int primaryKey, string newHashCode, IDictionary<int, string> hashCodeByPrimaryKey)
        {
            string formerHashCode = hashCodeByPrimaryKey.Where(kvp => kvp.Key.ToString() == primaryKey.ToString()).Select(kvp => kvp.Value).First();

            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            return comparer.Compare(newHashCode, formerHashCode) != 0;
        }

        /// <summary>
        /// reset cache for all tables which have the table from where entries were deleted
        /// as foreign key with delete option 'set null' or 'cascade'.
        /// </summary>
        /// <param name="tableName">The name of the table from where entries were deleted.</param>
        private void ResetCacheForRelatedTables(string tableName)
        {
            foreach (Type t in _objectByPrimaryKeyByTypeDictionary.Keys)
            {
                if (_metaDataRegister.TryGetValue(t, out MetaDataInfoTable tableInfo))
                {
                    MetaDataInfoColumn columnInfo = tableInfo.Columns.FirstOrDefault
                    (c => c.IsForeignKey && c.RelatedTableName == tableName &&
                        c.DeleteOption == DeleteOption.Cascade || c.DeleteOption == DeleteOption.SetNull);
                    if (columnInfo != null)
                    {
                        _objectByPrimaryKeyByTypeDictionary[t] = new ConcurrentDictionary<int, object>();
                    }
                }
            }
        }
    }
}
