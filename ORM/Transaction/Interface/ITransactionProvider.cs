﻿using System;
using System.Data;

namespace ORM.Transaction.Interface
{
    public interface ITransactionProvider : IDisposable
    {
        public void BeginTransaction();
        public void BeginTransaction(IsolationLevel isolationLevel);
        public void Commit();
        public void Rollback();
        public bool HasTransaction { get; }
    }
}
