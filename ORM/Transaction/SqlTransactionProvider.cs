﻿using System;
using System.Data;
using System.Data.SqlClient;
using ORM.Transaction.Interface;

namespace ORM.Transaction
{
    public class SqlTransactionProvider : ITransactionProvider
    {
        private readonly SqlConnection _sqlConnection;
        internal SqlTransaction SqlTransaction { get; private set; }

        public bool HasTransaction => SqlTransaction?.Connection != null;

        public SqlTransactionProvider(IDbConnection sqlConnection)
        {
            _sqlConnection = (SqlConnection)sqlConnection;
        }

        /// <summary>
        /// Begins a transaction.
        /// </summary>
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            if (HasTransaction)
            {
                throw new InvalidOperationException("Transaction has already started");
            }
            SqlTransaction = _sqlConnection.BeginTransaction(isolationLevel);
        }

        /// <summary>
        /// Begins a transaction.
        /// </summary>
        public void BeginTransaction()
        {
            if (HasTransaction)
            {
                throw new InvalidOperationException("Transaction has already started");
            }
            SqlTransaction = _sqlConnection.BeginTransaction();
        }


        /// <summary>
        /// Commits a transaction if existent.
        /// </summary>
        public void Commit()
        {
            if (!HasTransaction)
            {
               throw new InvalidOperationException("No transaction has started.");
            }
            SqlTransaction.Commit();
            SqlTransaction.Dispose();
        }

        /// <summary>
        /// Rolls back a transaction if existent.
        /// </summary>
        public void Rollback()
        {
            if (!HasTransaction)
            {
                throw new InvalidOperationException("No transaction has started.");
            }
            SqlTransaction.Rollback();
            SqlTransaction.Dispose();
        }


        /// <summary>
        /// Rolls back a transaction
        /// </summary>
        public void Dispose()
        {
            if (HasTransaction)
            {
                SqlTransaction.Rollback();
                SqlTransaction.Dispose();
            }
        }
    }
}
