﻿using System;
using System.Data;
using System.Data.SqlClient;
using ORM.DataAccess.Interface;

namespace ORM.DataAccess
{
    /// <summary>
    /// Represents a class for creating and connecting with the database.
    /// </summary>
    public static class DbFactory
    {
        /// <summary>
        /// Connects with the database.
        /// </summary>
        /// <returns>A new <see cref="SqlConnection"/>.</returns>
        public static IDbConnection GetConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }

        /// <summary>
        /// Creates a new database if not exists.
        /// </summary>
        /// <param name="dbName">The name of the database.</param>
        /// <param name="conn">The sql connection.</param>
        public static void CreateDb(string dbName, SqlConnection conn)
        {
            string cmdString = $"IF NOT EXISTS (SELECT * FROM SYS.DATABASES WHERE NAME = '{dbName}') CREATE DATABASE {dbName}";
            SqlCommand cmd = new SqlCommand(cmdString, conn);
            cmd.ExecuteNonQuery();
        }
    }
}
