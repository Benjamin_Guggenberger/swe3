﻿using System.Collections.Generic;
using System.Data;

namespace ORM.DataAccess.Interface
{
    public interface IQueryExecutor
    {
        public void ExecuteNonQuery(string query);
        public void ExecuteNonQuery(IDbCommand command);
        public IDataReader ExecuteReader(string query);
        public IDbCommand PrepareParameters(string query, IDictionary<string, object> valueByColumnNameDictionary);
    }
}