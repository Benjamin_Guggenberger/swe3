﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ORM.DataAccess.Interface;
using ORM.Transaction;
using ORM.Transaction.Interface;

namespace ORM.DataAccess
{
    /// <summary>
    /// Represents a class that interacts with the data base.
    /// </summary>
    public class SqlQueryExecutor : IQueryExecutor
    {
        private readonly SqlConnection _sqlConnection;
        private readonly SqlTransactionProvider _sqlTransactionProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlQueryExecutor"/> class.
        /// </summary>
        /// <param name="sqlConnection">The sql connection.</param>
        /// <param name="sqlTransactionProvider">The sql transaction.</param>
        public SqlQueryExecutor(IDbConnection sqlConnection, ITransactionProvider sqlTransactionProvider)
        {
            _sqlConnection = (SqlConnection)sqlConnection;
            _sqlTransactionProvider = (SqlTransactionProvider)sqlTransactionProvider;
        }

        /// <summary>
        /// Executes a sql query.
        /// </summary>
        /// <param name="query">The sql query that gets executed.</param>
        public void ExecuteNonQuery(string query)
        {
            var command = new SqlCommand(query, _sqlConnection, _sqlTransactionProvider.SqlTransaction);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Executes a sql query.
        /// </summary>
        /// <param name="command">The sql command to execute.</param>
        /// <returns>The id of the inserted object if not given before.</returns>
        public void ExecuteNonQuery(IDbCommand command)
        {
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Executes a sql select query.
        /// </summary>
        /// <param name="query">The sql query that gets executed.</param>
        /// <returns>An instance of the <see cref="IDataReader"/> class.</returns>
        public IDataReader ExecuteReader(string query)
        {
            var command = new SqlCommand(query, _sqlConnection, _sqlTransactionProvider.SqlTransaction);
            return command.ExecuteReader();
        }

        /// <summary>
        /// Prepares the parameter for an insert or update query.
        /// </summary>
        /// <param name="query">The sql query.</param>
        /// <param name="valueByColumnNameDictionary">A dictionary with the column name and its value as a key value pair</param>
        /// <returns>An instance of the <see cref="IDbCommand"/> class.</returns>
        public IDbCommand PrepareParameters(string query, IDictionary<string, object> valueByColumnNameDictionary)
        {
            var command = new SqlCommand(query, _sqlConnection, _sqlTransactionProvider.SqlTransaction);
            foreach (KeyValuePair<string, object> valueByColumnName in valueByColumnNameDictionary)
            {
                object value = valueByColumnName.Value ?? DBNull.Value;
                command.Parameters.AddWithValue(valueByColumnName.Key, value);
            }

            return command;
        }
    }
}
